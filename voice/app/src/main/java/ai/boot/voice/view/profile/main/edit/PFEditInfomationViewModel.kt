package ai.boot.voice.view.profile.main.edit

import ai.boot.voice.api.ApiBuilder
import ai.boot.voice.api.MessageModel
import ai.boot.voice.api.ResponseModel
import ai.boot.voice.base.BaseViewModel
import ai.boot.voice.model.InfomationProfileResponse
import android.util.ArrayMap
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

class PFEditInfomationViewModel: BaseViewModel() {
    private val userResponse = MutableLiveData<ResponseModel<InfomationProfileResponse>>()

    fun showUserInfo(): MutableLiveData<ResponseModel<InfomationProfileResponse>> {
        return userResponse
    }

    fun callApiPutUpdateInfo(gender: Int, firstname: String, lastname: String, birthday: String, mobile: String) {
        val jsonParams = JSONObject()
        if (birthday != "") {
            jsonParams.put("birth_date", birthday)
        }

        if (gender != 3) {
            jsonParams.put("gender", gender)
        }

        jsonParams.put("first_name", firstname)
        jsonParams.put("last_name", lastname)
        jsonParams.put("mobile", mobile)

        disposables.add(
            ApiBuilder.getWebService().putUpdateInfomation(jsonParams.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    userResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<InfomationProfileResponse>(message = message)
                    userResponse.value = response
                })
        )
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}