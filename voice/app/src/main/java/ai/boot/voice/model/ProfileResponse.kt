package ai.boot.voice.model

import android.os.Parcel
import android.os.Parcelable

class UploadAvatarResponse {
    var link: String? = null
    var file_path: String? = null
    var avatar: String? = null
}

class InfomationProfileResponse() : Parcelable {
    var last_name: String? = null
    var gender: Int? = null
    var first_name: String? = null
    var birth_date: String? = null
    var avatar: String? = null
    var mobile: String? = null
    var email: String? = null

    constructor(parcel: Parcel) : this() {
        last_name = parcel.readString()
        gender = parcel.readValue(Int::class.java.classLoader) as? Int
        first_name = parcel.readString()
        birth_date = parcel.readString()
        avatar = parcel.readString()
        mobile = parcel.readString()
        email = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(last_name)
        parcel.writeValue(gender)
        parcel.writeString(first_name)
        parcel.writeString(birth_date)
        parcel.writeString(avatar)
        parcel.writeString(mobile)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<InfomationProfileResponse> {
        override fun createFromParcel(parcel: Parcel): InfomationProfileResponse {
            return InfomationProfileResponse(parcel)
        }

        override fun newArray(size: Int): Array<InfomationProfileResponse?> {
            return arrayOfNulls(size)
        }
    }
}