package ai.boot.voice.view.newsfeed

import ai.boot.voice.R
import ai.boot.voice.model.ArticleModel
import ai.boot.voice.model.CategoryModel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class NewsfeedMainArticleAdapter(private var mList: List<ArticleModel>?) :
    RecyclerView.Adapter<NewsfeedMainArticleAdapter.ViewHolder>() {

    var onSavedArticle: ((Int) -> Unit)? = null
    var onClickArticle: ((Int) -> Unit)? = null

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_article_list_item, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val itemsViewModel = mList?.get(position)

        holder.titleArticle.text = itemsViewModel?.title
        holder.imageArticle.clipToOutline = true

//        Picasso.get().load(itemsViewModel?.image ?: "")
//            .error(R.color.petrol5)
//            .placeholder(R.color.petrol5)
//            .noFade()
//            .into(holder.imageArticle)

        holder.timeArticle.text = itemsViewModel?.created_date.toString()
        holder.imageArticleView.clipToOutline = true
        val isVideoOrTextType = itemsViewModel?.type == "text" || itemsViewModel?.type == "video"
        holder.btnSaved.setBackgroundResource(if (itemsViewModel?.status ?: false) R.drawable.ic_saved else R.drawable.ic_unsaved)
        holder.icTypeArticle.visibility =
            if (isVideoOrTextType) View.GONE else View.VISIBLE
        holder.imageArticleView.visibility = if (isVideoOrTextType) View.VISIBLE else View.GONE
        holder.icPlay.visibility = View.GONE
        if (itemsViewModel?.type == "pdf") {
            holder.icTypeArticle.setBackgroundResource(R.drawable.ic_audio)
        } else if (itemsViewModel?.type == "audio") {
            holder.icTypeArticle.setBackgroundResource(R.drawable.ic_audio)
        } else if (itemsViewModel?.type == "video") {
            holder.icPlay.setBackgroundResource(R.drawable.ic_play)
            holder.icPlay.visibility = View.VISIBLE
        }
        holder.icTypeArticle.background
        holder.titleDescription.text = itemsViewModel?.summary
        holder.btnSaved.setOnClickListener {
            onSavedArticle?.invoke(position)
        }
        holder.linearLayoutArticleContent.setBackgroundResource(if (position == 0) R.drawable.custom_view_search_category else R.color.petrol5)
    }

    // return the number of the items in the list
    override fun getItemCount(): Int = mList?.size ?: 0

    // Holds the views for adding it to image and text
    inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val titleArticle: TextView = itemView.findViewById(R.id.title_article)
        val imageArticle: ImageView = itemView.findViewById(R.id.image_article)
        val timeArticle: TextView = itemView.findViewById(R.id.title_time_article)
        val btnSaved: Button = itemView.findViewById(R.id.btn_saved)
        val titleDescription: TextView = itemView.findViewById(R.id.lb_description_article)
        val icTypeArticle: ImageView = itemView.findViewById(R.id.ic_type_article)
        val imageArticleView: RelativeLayout = itemView.findViewById(R.id.image_article_view)
        val icPlay: ImageView = itemView.findViewById(R.id.ic_play)
        val linearLayoutArticleContent: LinearLayout = itemView.findViewById(R.id.linearLayout_article_newsfeed)

        init {
            ItemView.setOnClickListener {
                onClickArticle?.invoke(adapterPosition)
            }
        }
    }

    fun reloadData(lstData: ArrayList<ArticleModel>) {
        mList = lstData
    }
}