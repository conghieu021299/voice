package ai.boot.voice.utils

import ai.boot.voice.common.Const
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import java.time.Duration
import java.time.temporal.Temporal
import java.util.*
import java.util.concurrent.TimeUnit
import android.content.Context
import android.provider.MediaStore

import android.graphics.Bitmap
import android.net.Uri
import java.io.ByteArrayOutputStream


open class Utils {
    companion object{

        fun checkToken() :Boolean{
            val token = SharePreferenceUtils.getInstances().getString(Const.KEY_TOKEN)
            Log.d("AAAAAAA",token.toString())
            if(token!=null &&  token.isNotEmpty()){
                return true
            }
            return false
        }

        fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
            val bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val path = MediaStore.Images.Media.insertImage(
                inContext.getContentResolver(),
                inImage,
                "Title",
                null
            )
            return Uri.parse(path)
        }
    }
}