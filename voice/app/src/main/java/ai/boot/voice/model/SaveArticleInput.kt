package ai.boot.voice.model

data class SaveArticleInput(
    var category_id: String
)
