package ai.boot.voice.view.newsfeed

import ai.boot.voice.R
import ai.boot.voice.model.ArticleModel
import ai.boot.voice.model.CategoriesModel
import ai.boot.voice.model.CategoryModel
import ai.boot.voice.model.TopicCategoryModel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

internal class NewsfeedCategoriesAdapter(private var mList: List<CategoryModel>) :
    RecyclerView.Adapter<NewsfeedCategoriesAdapter.ViewHolder>() {

    var onItemClick: ((Int) -> Unit)? = null
    var indexSelectedCategory = 0
    var mHolder: ViewHolder? = null

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_categories_newfeed_list_item, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        mHolder = holder
        val itemsViewModel = mList?.get(position)
        holder.titleCat.text = itemsViewModel.name
        holder.viewDotCat?.visibility = if (indexSelectedCategory == position) View.VISIBLE else View.GONE
        holder.viewCategory?.setBackgroundResource(if (indexSelectedCategory == position) R.drawable.shape_categories_main_newsfeed_selected else R.drawable.shape_categories_main_newsfeed_unselected)

        val param = holder.contentViewArticle.layoutParams as ViewGroup.MarginLayoutParams

        param.setMargins(if (position == 0) 45 else 25, 0,0,0)
        holder.itemView.layoutParams = param

//        holder.contentViewArticle.setMa
     }

    // return the number of the items in the list
    override fun getItemCount(): Int = mList?.size ?: 0

    // Holds the views for adding it to image and text
    internal inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val titleCat: TextView = itemView.findViewById(R.id.title_category)
        val viewCategory: RelativeLayout = itemView.findViewById(R.id.category_item_newsfeed)
        val viewDotCat: RelativeLayout = itemView.findViewById(R.id.dot_view_category)
        val contentViewArticle: LinearLayout = itemView.findViewById(R.id.contentview_article_newsfeed)

        init {
            ItemView.setOnClickListener {
                onItemClick?.invoke(adapterPosition)
            }
        }
    }

    fun reloadData(lstData: ArrayList<CategoryModel>) {
        mList = lstData
    }
}