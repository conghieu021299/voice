package ai.boot.voice.model


data class UserResponse(
        var status: Boolean,
        var error_message: String,
        var data: UserEntity,
        var error_code: String
)