package ai.boot.voice.view.profile.main.edit

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.common.extensions.*
import ai.boot.voice.customview.CustomBottomSheet
import ai.boot.voice.customview.DatePickerBottomSheet
import ai.boot.voice.model.InfomationProfileResponse
import ai.boot.voice.view.profile.main.ProfileMainFragmentViewModel
import ai.boot.voice.view.profile.main.infomation.GenderType
import ai.boot.voice.view.profile.main.infomation.PFInfomationActivity
import android.annotation.SuppressLint
import android.app.Activity
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_date_picker_bottom_sheet.*
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_pfedit_infomation.*
import kotlinx.android.synthetic.main.activity_pfedit_infomation.et_email
import kotlinx.android.synthetic.main.activity_pfedit_infomation.et_firstname
import kotlinx.android.synthetic.main.activity_pfedit_infomation.et_lastname
import kotlinx.android.synthetic.main.activity_pfinfomation.*
import kotlinx.android.synthetic.main.activity_pfinfomation.iv_back
import kotlinx.android.synthetic.main.activity_request_account.*
import java.time.LocalDate
import java.util.*

class PFEditInfomationActivity : BaseActivity() {

    private var profileModel: InfomationProfileResponse = InfomationProfileResponse()
    private var isChangedInfo: Boolean = false
    private lateinit var viewModel: PFEditInfomationViewModel
    private var isGenderNumber: Int = 3

    override fun getRootLayoutId(): Int {
        return R.layout.activity_pfedit_infomation
    }

    override fun setupView(savedInstanceState: Bundle?) {
        extraData {
            profileModel = it.getParcelable<InfomationProfileResponse>(Const.KEY_PROFILE_MODEL) ?: InfomationProfileResponse()
        }

        viewModel = ViewModelProviders.of(this).get(PFEditInfomationViewModel::class.java)
        setObserveLive(viewModel)

        viewModel.showUserInfo().observe(this, androidx.lifecycle.Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    if (it.data != null) {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        setUI()
        setAction()
        actionEditText()
    }

    private fun actionEditText() {
        et_telephone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                isChangedInfo = true
                isEnableButttonLogin()
            }
        })

        et_firstname.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                isChangedInfo = true
                et_firstname.removeTextChangedListener(this)
                isEnableButttonLogin()
                et_firstname.setText(et_firstname.text.toString().capitalizeFirstLetter())
                et_firstname.setSelection(s?.length ?: 0)
                et_firstname.addTextChangedListener(this)
                isEnableButttonLogin()
            }
        })

        et_lastname.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                isChangedInfo = true
                et_lastname.removeTextChangedListener(this)
                isEnableButttonLogin()
                et_lastname.setText(et_lastname.text.toString().capitalizeFirstLetter())
                et_lastname.setSelection(s?.length ?: 0)
                et_lastname.addTextChangedListener(this)
                isEnableButttonLogin()
            }
        })
    }

    private fun setAction() {
        btn_done.setOnClickListener {
            val firstname: String = (et_firstname.text ?: "").toString()
            val lastname: String = (et_lastname.text ?: "").toString()
            val birthday: String = (et_birthday.text ?: "").toString()
            if (birthday != "") {
                birthday.changeFormatDate(Const.DDMMYYYY, Const.YYYYMMDD)
            }
            val mobile: String = (et_telephone.text ?: "").toString()

            viewModel.callApiPutUpdateInfo(isGenderNumber, firstname, lastname, birthday, mobile)
        }

        iv_back.setOnClickListener {
            if (!isChangedInfo) {
                finish()
                return@setOnClickListener
            }
            val bottomSheet = CustomBottomSheet()
            bottomSheet.title = getString(R.string.abbrechen)
            bottomSheet.desc = getString(R.string.sind_sie_sicher_dass_sie_abbrechen_wollen_alle_nderungen_gehen_dabei_verloren)
            bottomSheet.titleButton = getString(R.string.ja_abbrechen)
            bottomSheet.titleCancelButton = getString(R.string.weiter_bearbeiten)
            bottomSheet.isTwoButton = true
            bottomSheet.didTouchButtonAction = {
                finish()
            }

            bottomSheet.didTouchButtonCancelAction = {
                bottomSheet.dismiss()
            }
            bottomSheet.show(supportFragmentManager, "bottomSheetBack")
        }

        btn_gender_male.setOnClickListener {
            isChangedInfo = true
            selectedGender(btn_gender_male)
        }

        btn_gender_female.setOnClickListener {
            isChangedInfo = true
            selectedGender(btn_gender_female)
        }

        btn_gender_other.setOnClickListener {
            isChangedInfo = true
            selectedGender(btn_gender_other)
        }

        iv_calendar.setOnClickListener {
            val calendarMax = Calendar.getInstance()
            calendarMax.add(Calendar.YEAR, -18)

            val calendarMin = Calendar.getInstance()
            calendarMin.set(Calendar.YEAR, 1800)

            var arrCurrentDate = et_birthday.text.toString().split(".")

            val picker = DatePickerBottomSheet()
            picker.maxDate = calendarMax.timeInMillis
            picker.minDate = calendarMin.timeInMillis

            picker.d = calendarMax.get(Calendar.DAY_OF_MONTH)
            picker.m = calendarMax.get(Calendar.MONTH)
            picker.y = calendarMax.get(Calendar.YEAR)

            if (arrCurrentDate.count() == 3) {
                picker.selectedDay = arrCurrentDate.get(0).toInt()
                picker.selectedMonth = arrCurrentDate.get(1).toInt()
                picker.selectedYear = arrCurrentDate.get(2).toInt()
            }

            picker.didTouchButtonAction = { day, month, year ->
                isChangedInfo = true
                val dateStr = "${day}.${month}.${year}"
                et_birthday.setText(dateStr.changeFormatDate("d.M.yyyy", Const.DDMMYYYY))
                picker.dismiss()
                isEnableButttonLogin()
            }
            picker.show(supportFragmentManager, "bottomSheetPicker")
        }
    }

    private fun setUI() {
        val firstname = profileModel.first_name
        val lastname = profileModel.last_name
        val email = profileModel.email
        val mobile = profileModel.mobile
        val birthday = profileModel.birth_date?.changeFormatDate(Const.YYYYMMDD, Const.DDMMYYYY)

        et_email.setText(email)
        et_birthday.setText(birthday)
        et_telephone.setText(mobile)
        et_firstname.setText(firstname)
        et_lastname.setText(lastname)
        et_birthday.isFocusable = false

        when (profileModel.gender) {
            0 -> selectedGender(btn_gender_male)
            1 -> selectedGender(btn_gender_female)
            2 -> selectedGender(btn_gender_other)
            else -> selectedGender(null)
        }
    }

    private fun selectedGender(buttonSelected: Button?) {
        val arrButton: MutableList<Button> = mutableListOf(btn_gender_male, btn_gender_female, btn_gender_other)
        for (i in arrButton) {
            if (i == buttonSelected) {
                i.setTextAppearance(this, R.style.EnableTextButtonGender)
                i.setBackgroundResource(R.drawable.background_primary_radius12)
            } else {
                i.setTextAppearance(this, R.style.DisableTextButtonGender)
                i.setBackgroundResource(R.drawable.background_gender_disable_profile)
            }
        }

        when (buttonSelected) {
            btn_gender_male -> isGenderNumber = 0
            btn_gender_female -> isGenderNumber = 1
            btn_gender_other -> isGenderNumber = 2
            else -> isGenderNumber = 3
        }

        isEnableButttonLogin()
    }

    private fun isEnableButttonLogin() {
        val firstname = et_firstname.text.toString()
        val lastname = et_lastname.text.toString()
        var isCheckSpaceFirstname: Boolean = false
        var isCheckSpaceLastname: Boolean = false
        if (firstname.removeAllSpace().count() > 0) {
            isCheckSpaceFirstname = true
        }

        if (lastname.removeAllSpace().count() > 0) {
            isCheckSpaceLastname = true
        }

        btn_done.isEnabled = (isChangedInfo && firstname.isNameValid() && lastname.isNameValid() && isCheckSpaceFirstname && isCheckSpaceLastname)
    }
}