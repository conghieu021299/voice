package ai.boot.voice.view.newfeed.category.search

import ai.boot.voice.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView

internal class SearchCategoryAdapter(private var itemsList: List<String>, private var isSearch: Boolean) : RecyclerView.Adapter<SearchCategoryAdapter.MyViewHolder>() {
    var onItemClick: ((String) -> Unit)? = null
    var onItemDeleteClick: ((String) -> Unit)? = null
    internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var itemTextView: TextView = view.findViewById(R.id.tv_key_search)
        var itemDelete: ImageView = view.findViewById(R.id.iv_delete)

        init {
            view.setOnClickListener {
                onItemClick?.invoke(itemsList[adapterPosition])
            }

            itemDelete.setOnClickListener {
                onItemDeleteClick?.invoke(itemsList[adapterPosition])
            }

            itemDelete.visibility = if (isSearch) { View.GONE } else { View.VISIBLE }
        }
    }
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.search_category_item, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = itemsList[position]
        holder.itemTextView.text = item
    }
    override fun getItemCount(): Int {
        return itemsList.size
    }
}