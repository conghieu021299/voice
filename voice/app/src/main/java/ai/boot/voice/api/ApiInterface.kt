package ai.boot.voice.api

import ai.boot.voice.model.*
import com.google.gson.JsonObject
import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by NguyenLinh on 02,October,2018
 */

interface ApiInterface {

    @POST("auth/signin")
    fun login(@Body dataLogin: InputLoginModel): Observable<ResponseModel<SigninResponse>>

    @POST("auth/token/refresh")
    fun refreshToken(@Body refresh: InputRefreshToken): Observable<ResponseModel<SigninResponse>>

    @POST("auth/token/refresh")
    fun postRefreshToken(@Body refresh: InputRefreshToken): Call<ResponseModel<SigninResponse>>

    @POST("auth/password/force-change")
    fun forceChangePassword(@Body dataLogin: InputForceChangePassword): Observable<ResponseModel<SigninResponse>>

    @GET("auth/recovery")
    fun getForgotPassword(@Query("email") email: String): Observable<MessageResponse>

    @POST("auth/recovery")
    fun confirmNewPassword(@Body data: InputForgotPassword): Observable<MessageResponse>

    @GET("newsfeed/categories/{categoryId}/articles")
    fun getArticlesById(@Path("categoryId") categoryId: String, @Query("page") page: Int, @Query("page_size") page_size: Int): Observable<ResponseModel<ArticleListModel>>

    @GET("newsfeed/categories/articles")
    fun getAllArticles(@Query("page") page: Int, @Query("page_size") page_size: Int): Observable<ResponseModel<ArticleListModel>>

    @GET("newsfeed/categories/selected")
    fun getAllSelectedCategories(): Observable<ResponseModel<CategoryListModel>>

    @GET("newsfeed/categories")
    fun getCategories(): Observable<ResponseModel<CategoryNFModel>>

    @GET("newsfeed/categories/history/search")
    fun getSearchHistoryCategory(): Observable<ResponseModel<CategoriesModel>>

    @GET("newsfeed/search")
    fun getSearchKeyword(@Query("keyword") keyword: String): Observable<ResponseModel<CategoriesModel>>

    @DELETE("newsfeed/categories/history/search/{id}")
    fun deleteKeywordHistory(@Path("id") id: String): Observable<MessageResponse>

    @DELETE("newsfeed/categories/history/search")
    fun deleteAllKeywordHistory(): Observable<MessageResponse>

    @POST("newsfeed/search")
    fun postSaveKeyword(@Body data: InputKeyword): Observable<MessageResponse>

    @POST("newsfeed/favorite/{categoryId}")
    fun addArticleToFavourite(@Path("categoryId") categoryId: String, @Body category: SaveArticleInput): Observable<MessageResponse>

    @DELETE("newsfeed/favorite/{categoryId}")
    fun removeArticleFromFavourite(@Path("categoryId") categoryId: String): Observable<MessageResponse>

    @POST("newsfeed/starts")
    fun postStartNewfeed(@Body data: InputStartNewfeed): Observable<MessageResponse>

    @POST("auth/signup")
    fun postSignup(@Body data: InputSignup): Observable<MessageResponse>

    @GET("upload/avatar")
    fun getLinkUploadAvatar(@Query("file_name") filename: String): Observable<ResponseModel<UploadAvatarResponse>>

//    @PUT()
//    fun uploadBinaryFile(@Body body: RequestBody?): Call<Void?>?

    @GET("newsfeed/favorite/categories/{categoryId}/articles")
    fun getFavoriteArticlesById(@Path("categoryId") categoryId: String, @Query("page") page: Int, @Query("page_size") page_size: Int): Observable<ResponseModel<ArticleListModel>>

    @GET("newsfeed/favorite/categories/articles")
    fun getAllFavoriteArticles(@Query("page") page: Int, @Query("page_size") page_size: Int): Observable<ResponseModel<ArticleListModel>>

    @GET("newsfeed/favorite/categories/selected")
    fun getFavoriteCategories(): Observable<ResponseModel<CategoryListModel>>

    @GET("profile")
    fun getInfomationProfile(): Observable<ResponseModel<InfomationProfileResponse>>

    @PUT("profile")
    fun putUpdateInfomation(@Body data: String): Observable<ResponseModel<InfomationProfileResponse>>
}