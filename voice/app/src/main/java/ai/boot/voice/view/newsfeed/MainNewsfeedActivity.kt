package ai.boot.voice.view.newsfeed

import ai.boot.voice.R
import ai.boot.voice.base.BaseFragment
import ai.boot.voice.common.Const
import ai.boot.voice.model.*
import ai.boot.voice.view.newfeed.category.CategoryNFActivity
import ai.boot.voice.view.newfeed.category.search.SearchCategoryActivity
import ai.boot.voice.view.reset.password.ResetPasswordActivity
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.todou.nestrefresh.LoadMoreFooterView
import com.todou.nestrefresh.RefreshHeaderView
import com.todou.nestrefresh.base.OnLoadMoreListener
import com.todou.nestrefresh.base.OnRefreshListener
import kotlinx.android.synthetic.main.activity_category_nfactivity.*
import kotlinx.android.synthetic.main.activity_main_newsfeed.*
import kotlinx.android.synthetic.main.activity_main_newsfeed_favourite.*
import kotlinx.android.synthetic.main.activity_search_category.*

class MainNewsfeedActivity : BaseFragment() {

    private lateinit var viewModel: MainNewsfeedViewModel
    var articles = ArrayList<ArticleModel>()
    private lateinit var adapter: NewsfeedMainArticleAdapter
    private lateinit var recycleView: RecyclerView

    var categories = ArrayList<CategoryModel>()
    private lateinit var adapterCategories: NewsfeedCategoriesAdapter
    private lateinit var recycleViewCategories: RecyclerView
    private lateinit var btnNext: Button
    private lateinit var nextView: RelativeLayout
    private var refreshView = activity?.let { SwipeRefreshLayout(it) }
    private var nestedSV = activity?.let { NestedScrollView(it) }
    private var loadingPB: ProgressBar? = null
    private var isLoadFullData = false
    private var indexSelectedCategory = 0

    companion object {
        fun newInstance(): MainNewsfeedActivity {
            return MainNewsfeedActivity()
        }
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_main_newsfeed
    }

    override fun setupViewModel() {
//        SharePreferenceUtils.getInstances().clearUserInfo()
        viewModel = ViewModelProviders.of(this).get(MainNewsfeedViewModel::class.java)
        setObserveLive(viewModel)

        adapter = NewsfeedMainArticleAdapter(articles)
        adapterCategories = NewsfeedCategoriesAdapter(categories)
        viewModel()

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun setupUI(view: View) {

        val broadCastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, intent: Intent?) {
                when (intent?.action) {
                    Const.SAVED_ARTICLE_FROM_SAVED_NEWSFEED -> loadArticleWithIndex(indexSelectedCategory)
                }
            }
        }
        viewModel.callApiGetAllArticles()
        viewModel.callApiGetAllCategories()
        refreshView = view?.findViewById<SwipeRefreshLayout>(R.id.refresh_layout_newsfeed)!!
        loadingPB = view?.findViewById<ProgressBar>(R.id.idPBLoading)
        nestedSV = view?.findViewById<NestedScrollView>(R.id.nested_scroll_newsfeed)
        btnNext = view?.findViewById<Button>(R.id.next_button_newsfeed)
        nextView = view?.findViewById<RelativeLayout>(R.id.view_next_button_next_category)
        nextView.visibility = View.GONE
        loadingPB?.visibility = View.GONE

        val layoutManagerArticle = object : LinearLayoutManager(activity){ override fun canScrollVertically(): Boolean{return true}}
        if (view?.findViewById<RecyclerView>(R.id.recyclerview_categories) != null) {
            recycleViewCategories =
                view?.findViewById<RecyclerView>(R.id.recyclerview_categories)!!
        }
        recycleViewCategories.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        if (view?.findViewById<RecyclerView>(R.id.recyclerview_newfeed) != null) {
            recycleView =
                view?.findViewById<RecyclerView>(R.id.recyclerview_newfeed)!!
            recycleView.isNestedScrollingEnabled = false
        }
        recycleView.layoutManager = layoutManagerArticle

        // REFRESH DATA
        refreshView?.setOnRefreshListener {
            refreshView?.isRefreshing = false
            viewModel.reloadData(categoryId = if (indexSelectedCategory == 0) "" else categories[indexSelectedCategory].id ?: "")
        }

        nestedSV?.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY == ((v as NestedScrollView).getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) && !isLoadFullData) {
                Log.d("Load more", "newsfeed123123")
                viewModel.loadMoreData(categoryId = if (indexSelectedCategory == 0) "" else categories[indexSelectedCategory].id ?: "")
                loadingPB?.visibility = View.VISIBLE
            }
        }
// call back

        btnNext.setOnClickListener {
            loadArticleWithIndex(if (indexSelectedCategory + 1 == categories.size) 0 else indexSelectedCategory + 1)
        }

        btn_topic_newsfeed.setOnClickListener {
            val intent = Intent(activity, CategoryNFActivity::class.java)
            intent.putExtra("isPushFromNewsfeed", true)
            startActivityForResult(intent, Const.CODE_CALLBACK_ONTOPIC_NEWSFEED)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Const.CODE_CALLBACK_ONTOPIC_NEWSFEED) {
            if (resultCode == Activity.RESULT_OK) {
//                val resultKey = data?.getStringExtra("KeywordSelected") ?: ""
//                val resultId = data?.getStringExtra("IdKeywordSelected") ?: ""
//
//                if (!resultKey.isEmpty() && !resultId.isEmpty()) {
//                    listYourTopic.add(TopicCategoryModel(resultId, resultKey))
//                    viewModel.callApiSaveKeyword(resultKey)
//                    tcl_your_topic.setTags(listYourTopic.map { it.name })
//                    autoSelectedCategory()
//                }
            }
        }
    }

    private fun viewModel() {

        viewModel.showArticles().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                loadingPB?.visibility = View.GONE
                if (it.data?.articles != null) {
                    if (viewModel.page == 1) {
                        articles = it.data?.articles ?: emptyList<ArticleModel>() as ArrayList<ArticleModel>
                    } else {
                        it.data?.articles.let { res -> articles.addAll((res.orEmpty()))}
                    }
                    reloadArticlesAdapter()
                    recycleView.adapter = adapter
                    adapter.notifyDataSetChanged()
                 }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.showCategories().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.data?.categories != null) {
                    categories =
                        (it.data?.categories ?: emptyList<CategoryModel>()) as ArrayList<CategoryModel>
                    val allCategory = CategoryModel(id = "", name = "alle Themen")
                    categories.add(0, allCategory)
                    reloadCategoriesAdapter()
                    adapterCategories.indexSelectedCategory = indexSelectedCategory
                    btnNext.setText(String.format("Nächstes Thema: ${if (indexSelectedCategory + 1 == categories.size) "alle Themen" else categories[indexSelectedCategory + 1].name}"))
                    recycleViewCategories.adapter = adapterCategories
                    adapterCategories.notifyDataSetChanged()
                }

            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.noticeIdArticleSavedSuccess().observe(this, Observer { articleId ->
            val index = articles.indexOfFirst { it.id == articleId }
            if (index > -1 && index < articles.size) {
                articles[index].status = true
                adapter.notifyItemChanged(index)
            }
        })

        viewModel.noticeIdArticleUnSavedSuccess().observe(this, Observer { articleId ->
            val index = articles.indexOfFirst { it.id == articleId }
            if (index > -1 && index < articles.size) {
                articles[index].status = false
                adapter.notifyItemChanged(index)
            }
        })

        viewModel.noticeLoadFullData().observe(this, {
            isLoadFullData = it
            nextView.visibility = if (isLoadFullData) View.VISIBLE else View.GONE
            loadingPB?.visibility = if (isLoadFullData) View.GONE else View.VISIBLE
        })

        adapterCategories.onItemClick = {
            loadArticleWithIndex(it)
        }

        adapter.onClickArticle = {
            var bundle = Bundle()
            bundle.putString(Const.TITLE_WEBVIEW, articles[it].title)
            bundle.putString(Const.LINK_WEBVIEW, articles[it].source)
            goToActivity(WebviewNewsfeedActivity::class.java, bundle = bundle)
        }

        adapter.onSavedArticle = {
            val article = articles[it]
            if (article.status ?: false) {
                viewModel.usSaveArticle(article.id ?: "")
            } else {
                viewModel.saveArticle(article.id ?: "", SaveArticleInput(article.categories?.first()?.id ?: ""))
            }
            articles[it].status = !(articles[it].status ?: true)
            val index = articles.indexOfFirst { it.id == article.id }
            if (index > -1 && index < articles.size) {
                adapter.notifyItemChanged(index)
            }
        }
    }

    private fun loadArticleWithIndex(index: Int) {
        val category = categories[index]
        indexSelectedCategory = index
        adapterCategories.indexSelectedCategory = indexSelectedCategory
        nestedSV?.fullScroll(View.FOCUS_UP)
        recycleViewCategories.smoothScrollToPosition(index)
        viewModel.resetPageNumber()
        if (index == 0) {
            viewModel.callApiGetAllArticles()
        } else {
            viewModel.callApiGetArticlesById(categoryId = category.id ?: "")
        }
        btnNext.setText(String.format("Nächstes Thema: ${if(indexSelectedCategory + 1 == categories.size) "alle Themen" else categories[indexSelectedCategory + 1].name}"))
        adapterCategories.notifyDataSetChanged()
    }

    private fun reloadCategoriesAdapter() {
        adapterCategories.reloadData(categories)
    }

    private fun reloadArticlesAdapter() {
        adapter.reloadData(articles)
    }
}