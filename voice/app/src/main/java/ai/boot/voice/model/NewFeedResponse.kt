package ai.boot.voice.model

data class CategoryNFModel(
    var top_topics: Array<TopicCategoryModel>? = null,
    var top_search_keywords: Array<TopicCategoryModel>? = null,
    var your_topics: Array<TopicCategoryModel>? = null
)

data class TopicCategoryModel(
    var id: String? = null,
    var name: String? = null
)

data class CategoriesModel (
    var categories: Array<ItemCategoriesModel>? = null
)

data class ItemCategoriesModel(
    var id: String? = null,
    var name: String? = null,
    var keyword: String? = null
)


