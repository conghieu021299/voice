package ai.boot.voice.view.newsfeed

import ai.boot.voice.R
import ai.boot.voice.model.ArticleModel
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class NewsfeedArticleFavouriteAdapter(private var mList: List<ArticleModel>?) :
    RecyclerView.Adapter<NewsfeedArticleFavouriteAdapter.ViewHolder>() {

    var onSavedArticle: ((Int) -> Unit)? = null
    var onClickArticle: ((Int) -> Unit)? = null

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_newsfeed_article_favourite, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val itemsViewModel = mList?.get(position)

        holder.titleArticle.text = itemsViewModel?.title
        holder.imageArticle.clipToOutline = true
        Picasso.get()
        Picasso.get().load(itemsViewModel?.image ?: "")
            .error(R.color.petrol5)
            .placeholder(R.color.petrol5)
            .noFade()
            .into(holder.imageArticle)

        val isVideoOrTextType = itemsViewModel?.type == "text" || itemsViewModel?.type == "video"
        holder.btnSaved.setBackgroundResource(if (itemsViewModel?.status ?: false) R.drawable.ic_saved else R.drawable.ic_unsaved)
        holder.icPlay.visibility = View.GONE
        if (itemsViewModel?.type == "pdf") {
            holder.imageArticle.setBackgroundResource(R.drawable.ic_audio)
        } else if (itemsViewModel?.type == "audio") {
            holder.imageArticle.setBackgroundResource(R.drawable.ic_audio)
        } else if (itemsViewModel?.type == "video") {
            holder.icPlay.setBackgroundResource(R.drawable.ic_play)
            holder.icPlay.visibility = View.VISIBLE
        }
        holder.btnSaved.setOnClickListener {
            onSavedArticle?.invoke(position)
        }
        holder.linearLayoutArticleContent.setBackgroundResource(if (position == 0) R.drawable.custom_view_search_category else R.color.petrol5)

    }

    // return the number of the items in the list
    override fun getItemCount(): Int = mList?.size ?: 0

    // Holds the views for adding it to image and text
    inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val titleArticle: TextView = itemView.findViewById(R.id.title_article_favorite)
        val imageArticle: ImageView = itemView.findViewById(R.id.image_article_favorite)
        val btnSaved: Button = itemView.findViewById(R.id.btn_saved_favorite)
        val icPlay: ImageView = itemView.findViewById(R.id.ic_play_favorite)
        val linearLayoutArticleContent: LinearLayout = itemView.findViewById(R.id.linearLayout_article_newsfeed_favorite)

        init {
            ItemView.setOnClickListener {
                onClickArticle?.invoke(adapterPosition)
            }
        }
    }

    fun reloadData(lstData: ArrayList<ArticleModel>) {
        mList = lstData
    }
}