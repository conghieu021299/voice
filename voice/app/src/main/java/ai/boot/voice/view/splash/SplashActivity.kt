package ai.boot.voice.view.splash

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.MainActivity
import ai.boot.voice.view.newfeed.category.CategoryNFActivity
import ai.boot.voice.view.signin.SinginActivity
import ai.boot.voice.view.wellcome.WellcomeActivity
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import java.util.*

@Suppress("DEPRECATION")
class SplashActivity : BaseActivity() {
    override fun getRootLayoutId(): Int {
        return R.layout.activity_splash
    }

    override fun setupView(savedInstanceState: Bundle?) {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val locale = Locale("de")
        val config: Configuration = baseContext.resources.configuration
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)

        Handler().postDelayed({
            checkLoginFlow()
            finish()
        }, 3000)
    }
}