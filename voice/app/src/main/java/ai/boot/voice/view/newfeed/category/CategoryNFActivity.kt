package ai.boot.voice.view.newfeed.category

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.customview.CustomBottomSheet
import ai.boot.voice.model.CategoryNFModel
import ai.boot.voice.model.InputStartNewfeed
import ai.boot.voice.model.TopicCategoryModel
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.MainActivity
import ai.boot.voice.view.newsfeed.MainNewsfeedActivity
import ai.boot.voice.view.newfeed.category.search.SearchCategoryActivity
import ai.boot.voice.view.signin.SigninViewModel
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import co.lujun.androidtagview.TagContainerLayout
import kotlinx.android.synthetic.main.activity_category_nfactivity.*
import kotlinx.android.synthetic.main.activity_wellcome.*
import co.lujun.androidtagview.TagView
import co.lujun.androidtagview.TagView.OnTagClickListener
import kotlinx.android.synthetic.main.activity_singin.*
import java.util.ArrayList
import kotlin.math.log


class CategoryNFActivity : BaseActivity() {

    private lateinit var viewModel: CategoryNFViewModel
    private var errorString: String = ""

    private var listTopTopic: MutableList<TopicCategoryModel> = mutableListOf()
    private var listTopSearchTopic: MutableList<TopicCategoryModel> = mutableListOf()
    private var listYourTopic: MutableList<TopicCategoryModel> = mutableListOf()
    private val minCategorySelected: Int = 3
    private var isPushFromNewsfeed = false

    override fun getRootLayoutId(): Int {
        return R.layout.activity_category_nfactivity
    }

    override fun setupView(savedInstanceState: Bundle?) {

        isPushFromNewsfeed = intent.getBooleanExtra("isPushFromNewsfeed", false)

        if (SharePreferenceUtils.getInstances().getCategoryFirstTime() == false) {
            btn_close.visibility = View.GONE
        }

        errorString = getString(R.string.message_some_thing_went_wrong)

        viewModel = ViewModelProviders.of(this).get(CategoryNFViewModel::class.java)
        setObserveLive(viewModel)

        viewModel.showCategories().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    if (it.data != null) {
                        setData(it.data)
                    } else {
                        showBottomSheet("Login fehlgeschlagen", it.message?.text_de, "Zurück zur Anmeldung")
                    }
                } else {
                    showBottomSheet("Login fehlgeschlagen", it.message?.text_de, "Zurück zur Anmeldung")
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.showStartNewfeed().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    SharePreferenceUtils.getInstances().saveCategoryFirstTime(true)
                    goToActivity(MainActivity::class.java)
                    finish()
                } else {
                    makeToastSomethingWentWrong()
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        btn_newfeed_start.setOnClickListener{
            val model = InputStartNewfeed(listYourTopic.map { it.id } as MutableList<String>)
            viewModel.callApiStartNewfeed(model)
        }

        viewModel.callApiGetCategory()

        tcl_top_tag.setOnTagClickListener(object : OnTagClickListener {
            override fun onTagClick(position: Int, text: String) {
                tagViewSelectedAction(tcl_top_tag, position, text)
            }
            override fun onTagLongClick(position: Int, text: String) { }
            override fun onSelectedTagDrag(position: Int, text: String) { }
            override fun onTagCrossClick(position: Int) { }
        })

        tcl_top_search.setOnTagClickListener(object : OnTagClickListener {
            override fun onTagClick(position: Int, text: String) {
                tagViewSelectedAction(tcl_top_search, position, text)
            }
            override fun onTagLongClick(position: Int, text: String) { }
            override fun onSelectedTagDrag(position: Int, text: String) { }
            override fun onTagCrossClick(position: Int) { }
        })

        tcl_your_topic.setOnTagClickListener(object : OnTagClickListener {
            override fun onTagClick(position: Int, text: String) { }
            override fun onTagLongClick(position: Int, text: String) { }
            override fun onSelectedTagDrag(position: Int, text: String) { }
            override fun onTagCrossClick(position: Int) {
                listTopTopic.indexOfFirst {
                    it.name == tcl_your_topic.getTagText(position)
                }.let {
                    if (it >= 0) {
                        tcl_top_tag.deselectTagView(it)
                    }
                }

                listTopSearchTopic.indexOfFirst {
                    it.name == tcl_your_topic.getTagText(position)
                }.let {
                    if (it >= 0) {
                        tcl_top_search.deselectTagView(it)
                    }
                }

                listYourTopic.removeAt(position)
                tcl_your_topic.setTags(listYourTopic.map { it.name })
                btn_newfeed_start.isEnabled = listYourTopic.count() >= minCategorySelected
            }
        })

        btn_search.setOnClickListener {
            val intent = Intent(this, SearchCategoryActivity::class.java)
            intent.putStringArrayListExtra("ListYourTopic", ArrayList(listYourTopic.map { it.name }))
            startActivityForResult(intent, Const.CODE_CALLBACK_SEARCHCATEGORY)
        }

        btn_close.setOnClickListener {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Const.CODE_CALLBACK_SEARCHCATEGORY) {
            if (resultCode == Activity.RESULT_OK) {
                val resultKey = data?.getStringExtra("KeywordSelected") ?: ""
                val resultId = data?.getStringExtra("IdKeywordSelected") ?: ""

                if (!resultKey.isEmpty() && !resultId.isEmpty()) {
                    listYourTopic.add(TopicCategoryModel(resultId, resultKey))
                    viewModel.callApiSaveKeyword(resultKey)
                    tcl_your_topic.setTags(listYourTopic.map { it.name })
                    autoSelectedCategory()
                }
            }
        }
    }

    private fun tagViewSelectedAction(tag: TagContainerLayout, position: Int, text: String) {
        val isSelected = tag.toggleSelectTagView(position)

        listTopTopic.indexOfFirst {
            it.name == tag.getTagText(position)
        }.let {
            if (it >= 0) {
                if (isSelected) { tcl_top_tag.selectTagView(it) } else { tcl_top_tag.deselectTagView(it) }
            }
        }

        listTopSearchTopic.indexOfFirst {
            it.name == tag.getTagText(position)
        }.let {
            if (it >= 0) {
                if (isSelected) { tcl_top_search.selectTagView(it) } else { tcl_top_search.deselectTagView(it) }
            }
        }

        if (isSelected) {
            var id: String = ""
            if (tag == tcl_top_tag) {
                id = listTopTopic.first { it.name == text }.id.toString()
            }

            if (tag == tcl_top_search) {
                id = listTopSearchTopic.first { it.name == text }.id.toString()
            }

            listYourTopic.add(TopicCategoryModel(id, text))
            tcl_your_topic.setTags(listYourTopic.map { it.name })
        } else {
            listYourTopic.indexOfFirst { it.name == text }.let {
                if (it >= 0) {
                    listYourTopic.removeAt(it)
                    tcl_your_topic.setTags(listYourTopic.map { it.name })
                }
            }
        }

        btn_newfeed_start.isEnabled = listYourTopic.count() >= minCategorySelected
    }

    private fun showBottomSheet(title: String, desc: String? = errorString, titleButton: String) {
        val bottomSheet = CustomBottomSheet()
        bottomSheet.title = title
        bottomSheet.desc = desc.toString()
        bottomSheet.titleButton = titleButton
        bottomSheet.didTouchButtonAction = {
            bottomSheet.dismiss()
        }
        bottomSheet.show(supportFragmentManager, "bottomSheet")
    }

    private fun setData(it: CategoryNFModel?) {
        it?.top_topics?.let {
            listTopTopic = it.toMutableList()
        }

        it?.top_search_keywords?.let {
            listTopSearchTopic = it.toMutableList()
        }

        it?.your_topics?.let {
            listYourTopic = it.toMutableList()
        }

        tcl_top_tag.setTags(listTopTopic.map { it.name })
        tcl_top_search.setTags(listTopSearchTopic.map { it.name })
        tcl_your_topic.setTags(listYourTopic.map { it.name })

        autoSelectedCategory()
     }

    private fun autoSelectedCategory() {
        if (listYourTopic.count() > 0) {
            for (i in listYourTopic) {
                listTopTopic.indexOfFirst {
                    it == i
                }.let {
                    if (it >= 0) {
                        tcl_top_tag.selectTagView(it)
                    }
                }

                listTopSearchTopic.indexOfFirst {
                    it == i
                }.let {
                    if (it >= 0) {
                        tcl_top_search.selectTagView(it)
                    }
                }
            }
        }

        btn_newfeed_start.isEnabled = listYourTopic.count() >= minCategorySelected
    }

    override fun onBackPressed() {
        finish()
    }
}