package ai.boot.voice.view.tabs

import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import ai.boot.voice.R
import ai.boot.voice.base.BaseFragment


class Home3Fragment : BaseFragment() {

    companion object {
        fun newInstance(): Home3Fragment {
            return Home3Fragment()
        }
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_home3
    }

    override fun setupViewModel() {
        Log.d(TAG, "setupViewModel")
        //0.0 Config UserViewModel

    }


    override fun setupUI(view: View) {
        setTitleToolbar("Home2")
    }
}
