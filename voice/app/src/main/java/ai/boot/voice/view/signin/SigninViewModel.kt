package ai.boot.voice.view.signin

import ai.boot.voice.api.ApiBuilder
import ai.boot.voice.api.MessageModel
import ai.boot.voice.api.ResponseModel
import ai.boot.voice.base.BaseViewModel
import ai.boot.voice.model.InputLoginModel
import ai.boot.voice.model.SigninResponse
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SigninViewModel: BaseViewModel() {
    private val userResponse = MutableLiveData<ResponseModel<SigninResponse>>()

    fun showUserInfo(): MutableLiveData<ResponseModel<SigninResponse>> {
        return userResponse
    }

    fun callApiLogin(inputLogin: InputLoginModel) {
        disposables.add(
            ApiBuilder.getWebService().login(inputLogin)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    userResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<SigninResponse>(message = message)
                    userResponse.value = response
                })
        )
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}