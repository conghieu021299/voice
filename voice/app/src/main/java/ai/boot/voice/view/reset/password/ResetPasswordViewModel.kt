package ai.boot.voice.view.reset.password

import ai.boot.voice.api.ApiBuilder
import ai.boot.voice.api.MessageModel
import ai.boot.voice.api.MessageResponse
import ai.boot.voice.api.ResponseModel
import ai.boot.voice.base.BaseViewModel
import ai.boot.voice.model.InputForceChangePassword
import ai.boot.voice.model.InputForgotPassword
import ai.boot.voice.model.SigninResponse
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ResetPasswordViewModel: BaseViewModel() {
    private val forcePasswordResponse = MutableLiveData<ResponseModel<SigninResponse>>()
    private val confirmNewPasswordResponse = MutableLiveData<MessageResponse>()

    fun showForceChangePassword(): MutableLiveData<ResponseModel<SigninResponse>> {
        return forcePasswordResponse
    }

    fun showConfirmNewPassword(): MutableLiveData<MessageResponse> {
        return confirmNewPasswordResponse
    }

    fun callApiForceChangePassword(inputForceChange: InputForceChangePassword) {
        disposables.add(
            ApiBuilder.getWebService().forceChangePassword(inputForceChange)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    forcePasswordResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<SigninResponse>(message = message)
                    forcePasswordResponse.value = response
                })
        )
    }

    fun callApiConfirmNewPassword(input: InputForgotPassword) {
        disposables.add(
            ApiBuilder.getWebService().confirmNewPassword(input)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    confirmNewPasswordResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = MessageResponse(message = message)
                    confirmNewPasswordResponse.value = response
                })
        )
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}