package ai.boot.voice.view.signin

import ai.boot.voice.BuildConfig
import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.common.extensions.isEmailValid
import ai.boot.voice.common.extensions.isInputEmailValid
import ai.boot.voice.common.extensions.isValidPassword
import ai.boot.voice.common.extensions.removeSpaceChar
import ai.boot.voice.customview.CustomBottomSheet
import ai.boot.voice.model.InputLoginModel
import ai.boot.voice.model.SigninResponse
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.MainActivity
import ai.boot.voice.view.forgot.password.ForgotPasswordActivity
import ai.boot.voice.view.newsfeed.MainNewsfeedActivity
import ai.boot.voice.view.request.account.RequestAccountActivity
import ai.boot.voice.view.reset.password.ResetPasswordActivity
import ai.boot.voice.view.wellcome.WellcomeActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_singin.*


class SinginActivity : BaseActivity() {
    private lateinit var viewModel: SigninViewModel
    private var email: String = ""
    private var password: String = ""
    private var isShowPassword: Boolean = false
    private var errorString: String = ""

    override fun getRootLayoutId(): Int {
        return R.layout.activity_singin
    }

    override fun setupView(savedInstanceState: Bundle?) {
        errorString = getString(R.string.message_some_thing_went_wrong)

        viewModel = ViewModelProviders.of(this).get(SigninViewModel::class.java)
        setObserveLive(viewModel)
        tv_version_app.text = BuildConfig.FLAVOR + " " + BuildConfig.VERSION_NAME

        viewModel.showUserInfo().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    if (it.data != null) {
                        setupLogin(it.data)
                    } else {
                        showBottomSheet("Login fehlgeschlagen", it.message?.text_de, "Zurück zur Anmeldung")
                    }
                } else {
                    showBottomSheet("Login fehlgeschlagen", it.message?.text_de, "Zurück zur Anmeldung")
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        btn_signin.setOnClickListener {
            if (isValidUsername() && isValidPassword()) {
                callAPILogin()
            }
        }

        iv_icon_password.setOnClickListener {
            if (isShowPassword) {
                iv_icon_password.setImageResource(R.drawable.ic_eye_show)
                et_password.transformationMethod = PasswordTransformationMethod.getInstance()
            } else {
                iv_icon_password.setImageResource(R.drawable.ic_eye_slash)
                et_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }
            isShowPassword = !isShowPassword
        }

        et_username.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            isValidUsername()
            hideKeyboard()
            return@OnEditorActionListener true
        })

        et_password.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            isValidPassword()
            hideKeyboard()
            return@OnEditorActionListener true
        })

        tv_forgot_password.setOnClickListener {
            SharePreferenceUtils.getInstances().saveEmail(et_username.text.toString().trim())
            val bundle = Bundle()
            bundle.putBoolean(Const.FORGOT_PASSWORD, true)
            goToActivity(ForgotPasswordActivity::class.java, bundle = bundle)
        }

        tv_request_account.setOnClickListener {
            goToActivity(RequestAccountActivity::class.java)
        }

        setOnFocus(et_username, tv_username_error)
        setOnFocus(et_password, tv_password_error)
        addTextChanged(et_username)
        addTextChanged(et_password)
        et_password.removeSpaceChar(16)
    }

    private fun setOnFocus(et: EditText, tv: TextView) {
        et.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                et.setBackgroundResource(R.drawable.custom_input_login)
                tv.visibility = View.GONE
                if (et == et_username) {
                    iv_icon_username.visibility = View.GONE
                }
            }
        }
    }

    private fun addTextChanged(et: EditText) {
        et.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                isEnableButttonLogin()
            }
        })
    }

    private fun isValidUsername(): Boolean {
        email = et_username.text.toString().trim()
        iv_icon_username.visibility = View.VISIBLE
        if (email.count() < 3 || !email.isEmailValid()) {
            tv_username_error.visibility = View.VISIBLE
            tv_username_error.text = getString(R.string.message_1)
            et_username.setBackgroundResource(R.drawable.input_login_error)
            iv_icon_username.setImageResource(R.drawable.ic_x_red)
            return false
        } else {
            tv_username_error.visibility = View.GONE
            et_username.setBackgroundResource(R.drawable.custom_input_login)
            iv_icon_username.setImageResource(R.drawable.ic_check_white)
            return true
        }
    }

    private fun isValidPassword(): Boolean {
        password = et_password.text.toString().trim()
        if (password.count() < 8 || !password.isValidPassword()) {
            tv_password_error.visibility = View.VISIBLE
            tv_password_error.text = getString(R.string.message_3)
            et_password.setBackgroundResource(R.drawable.input_login_error)
            return false
        } else {
            tv_password_error.visibility = View.GONE
            et_password.setBackgroundResource(R.drawable.custom_input_login)
            return true
        }
    }

    private fun isEnableButttonLogin() {
        email = et_username.text.toString().trim()
        password = et_password.text.toString().trim()
        btn_signin.isEnabled = email.isNotEmpty() && password.isNotEmpty()
    }

    fun callAPILogin() {
        email = et_username.text.toString().trim()
        password = et_password.text.toString().trim()

        viewModel.callApiLogin(InputLoginModel(email, password))
    }


    private fun setupLogin(it: SigninResponse?) {
        if (it != null) {
            SharePreferenceUtils.getInstances().saveEmail(et_username.text.toString().trim())
            if (it.challengename == Const.NEW_PASSWORD_REQUIRED) {
                it.session?.let { it1 -> SharePreferenceUtils.getInstances().saveSession(it1) }
                val bundle = Bundle()
                bundle.putBoolean(Const.FORGOT_PASSWORD, false)
                goToActivity(ResetPasswordActivity::class.java, bundle = bundle)
            } else {
                it.id_token?.let { it1 -> SharePreferenceUtils.getInstances().saveIdToken(it1) }
                it.refresh_token?.let { it1 -> SharePreferenceUtils.getInstances().saveRefreshToken(it1) }
                SharePreferenceUtils.getInstances().saveUserInfor(it)
                checkLoginFlow()
            }
        }
    }

    private fun showBottomSheet(title: String, desc: String? = errorString, titleButton: String) {
        val bottomSheet = CustomBottomSheet()
        bottomSheet.title = title
        bottomSheet.desc = desc.toString()
        bottomSheet.titleButton = titleButton
        bottomSheet.didTouchButtonAction = {
            bottomSheet.dismiss()
        }
        bottomSheet.show(supportFragmentManager, "bottomSheet")
    }
    override fun onBackPressed() {
        finish()
    }
}
