package ai.boot.voice.customview

import ai.boot.voice.R
import android.app.Dialog
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.FrameLayout
import androidx.annotation.RequiresApi
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_date_picker_bottom_sheet.*
import kotlinx.android.synthetic.main.activity_pfedit_infomation.*
import java.util.*

class DatePickerBottomSheet : BottomSheetDialogFragment() {

    var didTouchButtonAction: ((String, String, String) -> Unit)? = null
    var maxDate: Long = 0
    var minDate: Long = 0

    var selectedDay: Int = 0
    var selectedMonth: Int = 0
    var selectedYear: Int = 0

    var y: Int = 0
    var m: Int = 0
    var d: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_date_picker_bottom_sheet, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                val bottomSheet = findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                bottomSheet.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.setBackgroundColor(Color.TRANSPARENT)
        datePicker.maxDate = maxDate
        datePicker.minDate = minDate

        if (selectedDay != 0 && selectedMonth != 0 && selectedYear != 0) {
            datePicker.updateDate(selectedYear, selectedMonth - 1, selectedDay)
            y = selectedYear
            m = selectedMonth
            d = selectedDay
        } else {
            datePicker.updateDate(y, m, d)
        }

        btn_accept.setOnClickListener {
            didTouchButtonAction?.invoke(d.toString(), m.toString() ,y.toString())
        }

        datePicker.setOnDateChangedListener { _, year, monthOfYear, dayOfMonth ->
            y = year
            m = monthOfYear + 1
            d = dayOfMonth
        }
    }
}