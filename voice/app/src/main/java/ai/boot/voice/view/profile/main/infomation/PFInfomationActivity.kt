package ai.boot.voice.view.profile.main.infomation

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.common.extensions.CircleTransform
import ai.boot.voice.common.extensions.changeFormatDate
import ai.boot.voice.model.InfomationProfileResponse
import ai.boot.voice.model.TopicCategoryModel
import ai.boot.voice.view.newfeed.category.search.SearchCategoryActivity
import ai.boot.voice.view.profile.main.ProfileMainFragmentViewModel
import ai.boot.voice.view.profile.main.edit.PFEditInfomationActivity
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_category_nfactivity.*
import kotlinx.android.synthetic.main.activity_pfinfomation.*
import kotlinx.android.synthetic.main.activity_pfinfomation.iv_avatar
import kotlinx.android.synthetic.main.activity_pfinfomation.tv_fullname
import java.util.ArrayList

enum class GenderType(val gender: Int) {
    MALE(R.string.männlich),
    FEMALE(R.string.weiblich),
    OTHER(R.string.divers)
}

class PFInfomationActivity : BaseActivity() {

    private var profileModel: InfomationProfileResponse = InfomationProfileResponse()
    private lateinit var viewModel: ProfileMainFragmentViewModel

    override fun getRootLayoutId(): Int {
        return R.layout.activity_pfinfomation
    }

    override fun setupView(savedInstanceState: Bundle?) {
        extraData {
            profileModel = it.getParcelable<InfomationProfileResponse>(Const.KEY_PROFILE_MODEL) ?: InfomationProfileResponse()
        }

        viewModel = ViewModelProviders.of(this).get(ProfileMainFragmentViewModel::class.java)
        setObserveLive(viewModel)

        viewModel.showUserInfo().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    if (it.data != null) {
                        profileModel = it.data!!
                        setData()
                    }
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        setAction()
        setData()
    }

    private fun setAction() {
        iv_back.setOnClickListener {
            finish()
        }

        btn_edit_infomation.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelable(Const.KEY_PROFILE_MODEL, profileModel)

            val intent = Intent(this, PFEditInfomationActivity::class.java)
            intent.putExtra(Const.KEY_EXTRA_DATA, bundle)
            startActivityForResult(intent, Const.CODE_CALLBACK_CHANGEDINFO)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Const.CODE_CALLBACK_CHANGEDINFO) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.calApiGetInfomationProfileLoading()
            }
        }
    }

    private fun setData() {
        val firstname = profileModel.first_name
        val lastname = profileModel.last_name
        val image = profileModel.avatar
        val email = profileModel.email
        tv_fullname.setText("${firstname}\n${lastname}")
        tv_email.setText(email)
        Picasso.get().load(image)
            .resize(120, 120)
            .placeholder(R.mipmap.avatar_placeholder)
            .error(R.mipmap.avatar_placeholder)
            .transform(CircleTransform())
            .centerCrop().into(iv_avatar)

        when (profileModel.gender) {
            0 -> tv_gender.text = getString(GenderType.MALE.gender)
            1 -> tv_gender.text = getString(GenderType.FEMALE.gender)
            2 -> tv_gender.text = getString(GenderType.OTHER.gender)
            else -> view_gender.visibility = View.GONE
        }

        if (profileModel.birth_date == null) {
            tv_birthday.setTextAppearance(this, R.style.PlaceholderTextSize15)
        } else {
            tv_birthday.text = profileModel.birth_date?.changeFormatDate(Const.YYYYMMDD, Const.DDMMYYYY)
            tv_birthday.setTextAppearance(this, R.style.NormalTextSize15)
        }

        if (profileModel.mobile == null) {
            tv_telephone.setTextAppearance(this, R.style.PlaceholderTextSize15)
        } else {
            tv_telephone.text = profileModel.mobile
            tv_telephone.setTextAppearance(this, R.style.NormalTextSize15)
        }
    }
}