package ai.boot.voice.view.newsfeed

import ai.boot.voice.api.ApiBuilder
import ai.boot.voice.api.MessageModel
import ai.boot.voice.api.ResponseModel
import ai.boot.voice.base.BaseViewModel
import ai.boot.voice.model.*
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainNewsfeedFavouriteModel: BaseViewModel() {
    private val articles = MutableLiveData<ResponseModel<ArticleListModel>>()
    private val categories = MutableLiveData<ResponseModel<CategoryListModel>>()
    private val isLoadingFullData = MutableLiveData<Boolean>()
    private val idArticleSavedSuccess = MutableLiveData<String>()
    private val idArticleUnSavedSuccess = MutableLiveData<String>()
    private var isFirstload = true
    private val idCategoriyHaveNoArticleSaved = MutableLiveData<String>()

    var page = 0
    private  var pageSize = 20

    fun showArticles(): MutableLiveData<ResponseModel<ArticleListModel>> {
        return articles
    }

    fun showCategories(): MutableLiveData<ResponseModel<CategoryListModel>> {
        return categories
    }

    fun noticeLoadFullData(): MutableLiveData<Boolean> {
        return isLoadingFullData
    }

    fun noticeIdArticleSavedSuccess(): MutableLiveData<String> {
        return idArticleSavedSuccess
    }

    fun noticeIdArticleUnSavedSuccess(): MutableLiveData<String> {
        return idArticleUnSavedSuccess
    }

    fun noticeIdCategoryHaveNoSavedArticles(): MutableLiveData<String> {
        return idCategoriyHaveNoArticleSaved
    }

    fun loadMoreData(categoryId: String) {
        if (categoryId.isEmpty()) {
            callApiGetAllFavouriteArticles()
        } else {
            callApiGetArticlesById(categoryId = categoryId)
        }
    }

    fun reloadData(categoryId: String) {
        resetPageNumber()
        if (categoryId.isEmpty()) {
            callApiGetAllFavouriteArticles()
        } else {
            callApiGetArticlesById(categoryId = categoryId)
        }
        callApiGetAllCategories()
    }

    fun callApiGetAllFavouriteArticles() {

        updatePageNumber()
        disposables.add(
            ApiBuilder.getWebService().getAllFavoriteArticles(page, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { if (!isFirstload) {
                    showLoading(true)
                } }
                .doFinally { showLoading(false) }
                .subscribe({

                    isFirstload = false
                    articles.value = it
                    isLoadingFullData.value = (it.data?.articles?.size ?: 0 < pageSize)

                }, {
                    isFirstload = false
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<ArticleListModel>(message = message)
                    articles.value = response
                })
        )
    }

    fun callApiGetArticlesById(categoryId: String) {

        updatePageNumber()
        disposables.add(
            ApiBuilder.getWebService().getFavoriteArticlesById(categoryId = categoryId,page, pageSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({

                    articles.value = it
                    isLoadingFullData.value = (it.data?.articles?.size ?: 0 < pageSize)
                    if (page == 1 && articles?.value?.data?.articles?.size == 0 && !categoryId.isEmpty()) {
                        isFirstload = true
                        idCategoriyHaveNoArticleSaved.value = categoryId
                    }

                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<ArticleListModel>(message = message)
                    articles.value = response
                })
        )
    }

    fun saveArticle(articleId: String, categoryModel: SaveArticleInput) {
        disposables.add(
            ApiBuilder.getWebService().addArticleToFavourite(categoryId = articleId, categoryModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.message?.status == "success") {
                        idArticleSavedSuccess.value = articleId
                    } else {
                        idArticleUnSavedSuccess.value = articleId
                    }
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<CategoryListModel>(message = message)
                    idArticleUnSavedSuccess.value = articleId
                })
        )
    }

    fun usSaveArticle(articleId: String) {
        disposables.add(
            ApiBuilder.getWebService().removeArticleFromFavourite(categoryId = articleId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.message?.status == "success") {
                        idArticleUnSavedSuccess.value = articleId
                    } else {
                        idArticleSavedSuccess.value = articleId
                    }
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<CategoryListModel>(message = message)
                    idArticleSavedSuccess.value = articleId
                })
        )
    }

    fun callApiGetAllCategories() {
        disposables.add(
            ApiBuilder.getWebService().getFavoriteCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    categories.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<CategoryListModel>(message = message)
                    categories.value = response
                })
        )
    }

    private fun updatePageNumber() {
        page += 1
    }

    fun resetPageNumber() {
        page = 0
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}