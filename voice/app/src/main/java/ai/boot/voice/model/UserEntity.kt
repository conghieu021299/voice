package ai.boot.voice.model

data class UserEntity(
    var id: Int,
    var name: String,
    var lastName: String
)