package ai.boot.voice.view.tabs

import android.view.View
import ai.boot.voice.R
import ai.boot.voice.base.LazyLoadFragment


class Home2Fragment : LazyLoadFragment() {



    companion object {
        fun newInstance(): Home2Fragment {
            return Home2Fragment()
        }
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_home2
    }

    override fun setupViewModel() {

    }



    override fun setupUI(view: View) {
        setTitleToolbar(getString(R.string.home1))
    }

    override fun loadData() {

    }

}
