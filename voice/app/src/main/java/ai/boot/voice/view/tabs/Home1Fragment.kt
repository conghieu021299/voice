package ai.boot.voice.view.tabs

import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import ai.boot.voice.R
import ai.boot.voice.adapter.RepositorieAdapter
import ai.boot.voice.base.BaseFragment
import ai.boot.voice.model.RepositoriesEntity
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.signin.SinginActivity
import kotlinx.android.synthetic.main.fragment_home.*
import android.content.Intent
import androidx.fragment.app.FragmentManager


class Home1Fragment : BaseFragment() {

    companion object {
        fun newInstance(): Home1Fragment {
            return Home1Fragment()
        }
    }

    override fun getRootLayoutId(): Int {
        return R.layout.fragment_home
    }

    override fun setupViewModel() {
        Log.d(TAG, "setupViewModel")
        //0.0 Config UserViewModel

        btn_logout.setOnClickListener {
            SharePreferenceUtils.getInstances().clearUserInfo()
            goToActivity(SinginActivity::class.java)
        }
    }

    private fun setupRepositories(lists: List<RepositoriesEntity>?) {
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rcvList.layoutManager = layoutManager
        rcvList.adapter = RepositorieAdapter(lists)
    }


    override fun setupUI(view: View) {

    }
}
