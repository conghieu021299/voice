package ai.boot.voice.customview

import ai.boot.voice.R
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottomsheet_custom.*

class CustomBottomSheet: BottomSheetDialogFragment() {

    var title: String = ""
    var desc: String = ""
    var titleButton: String = ""
    var titleCancelButton: String = ""
    var titleAcceptButton2: String = ""
    var isTwoButton: Boolean = false
    var isUsingTwoAcceptButton: Boolean = false
    var didTouchButtonAction: (() -> Unit)? = null
    var didTouchButtonCancelAction: (() -> Unit)? = null
    var didTouchButtonAccept2Action: (() -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottomsheet_custom, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                val bottomSheet = findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                bottomSheet.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_accept2_bottomsheet.visibility = if (isUsingTwoAcceptButton) { View.VISIBLE } else { View.GONE }
        btn_cancel_bottomsheet.visibility = if (isTwoButton) { View.VISIBLE } else { View.GONE }
        view.setBackgroundColor(Color.TRANSPARENT)
        tv_title_bottomsheet.text = title
        tv_desc_bottomsheet.text = desc
        btn_accept2_bottomsheet.setText(titleAcceptButton2)
        btn_accept_bottomsheet.setText(titleButton)
        btn_cancel_bottomsheet.setText(titleCancelButton)

        btn_accept_bottomsheet.setOnClickListener {
            didTouchButtonAction?.invoke()
        }

        btn_cancel_bottomsheet.setOnClickListener {
            didTouchButtonCancelAction?.invoke()
        }

        btn_accept2_bottomsheet.setOnClickListener {
            didTouchButtonAccept2Action?.invoke()
        }
    }
}