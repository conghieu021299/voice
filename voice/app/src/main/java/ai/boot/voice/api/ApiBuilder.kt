package ai.boot.voice.api


import ai.boot.voice.BuildConfig
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by HoangLV
 */
class ApiBuilder() {
    companion object {


        private val apiInterface: ApiInterface? = null
        fun getWebService(): ApiInterface {
            if (apiInterface != null) {
                return apiInterface
            }

            val retrofit = Retrofit.Builder().baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okhttpClient())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }

        fun getWebServiceWithLink(link: String): ApiInterface {
            if (apiInterface != null) {
                return apiInterface
            }

            val retrofit = Retrofit.Builder().baseUrl(link)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okhttpClient())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }

        private fun okhttpClient(): OkHttpClient {
            val dispatcher = Dispatcher()
            dispatcher.maxRequests = 1
            val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
            okHttpClientBuilder.dispatcher(dispatcher)
            okHttpClientBuilder.addInterceptor(APIInterceptor(this))
            return okHttpClientBuilder.build()
        }
    }
}