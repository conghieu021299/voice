package ai.boot.voice.view.wellcome

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.MainActivity
import ai.boot.voice.view.newfeed.category.CategoryNFActivity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_wellcome.*

class WellcomeActivity : BaseActivity() {
    override fun getRootLayoutId(): Int {
        return R.layout.activity_wellcome
    }

    override fun setupView(savedInstanceState: Bundle?) {

        btn_next.setOnClickListener {
            val email: String = SharePreferenceUtils.getInstances().getEmail() ?: ""
            SharePreferenceUtils.getInstances().saveListEmailLogin(email)
            SharePreferenceUtils.getInstances().saveWellcomeFirstTime(true)
            if (SharePreferenceUtils.getInstances().getCategoryFirstTime() == true) {
                goToActivity(MainActivity::class.java)
            } else {
                goToActivity(CategoryNFActivity::class.java)
            }
            finish()
        }
    }
}