package ai.boot.voice.model

data class ArticleListModel (
    var articles: ArrayList<ArticleModel>? = null
        )
data class ArticleModel (
    var status: Boolean? = false,
    var created_date: Double? = null,
    var image: String? = null,
    var view: Int? = null,
    var type: String? = null,
    var title: String? = null,
    var summary: String? = null,
    var source: String? = null,
    var keyword: String? = null,
    var id: String? = null,
    var categories: ArrayList<CategoryModel>? = null,
)

data class CategoryModel (
    var id: String? = null,
    var name: String? = null,
    )

data class CategoryListModel (
    var categories: ArrayList<CategoryModel>? = null,
)