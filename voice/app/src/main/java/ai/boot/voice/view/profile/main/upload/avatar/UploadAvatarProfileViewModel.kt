package ai.boot.voice.view.profile.main.upload.avatar

import ai.boot.voice.BuildConfig
import ai.boot.voice.api.*
import ai.boot.voice.base.BaseViewModel
import ai.boot.voice.model.InputSignup
import ai.boot.voice.model.UploadAvatarResponse
import android.R.attr
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import android.R.attr.direction
import android.content.Context
import android.net.Uri

import okhttp3.MediaType

import okhttp3.RequestBody
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import android.os.FileUtils





class UploadAvatarProfileViewModel: BaseViewModel() {
    private val userResponse = MutableLiveData<ResponseModel<UploadAvatarResponse>>()

    fun showUserInfo(): MutableLiveData<ResponseModel<UploadAvatarResponse>> {
        return userResponse
    }

    fun callApiGetLinkUpload(file: Uri) {
        disposables.add(
            ApiBuilder.getWebService().getLinkUploadAvatar("avatar")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .subscribe({
                    callApiPutUploadAvatar(it.data?.avatar ?: "", file)
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<UploadAvatarResponse>(message = message)
                    userResponse.value = response
                })
        )
    }

    fun callApiPutUploadAvatar(link: String, file: Uri) {

//        disposables.add(
//            ApiBuilder.getWebServiceWithLink(link).uploadBinaryFile()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe { showLoading(true) }
//                .subscribe({
//                    userResponse.value = it
//                }, {
//                    val message = MessageModel(id = "-1")
//                    val response = ResponseModel<UploadAvatarResponse>(message = message)
//                    userResponse.value = response
//                })
//        )
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}