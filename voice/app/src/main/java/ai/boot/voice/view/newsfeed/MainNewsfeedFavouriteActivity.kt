package ai.boot.voice.view.newsfeed

import ai.boot.voice.R
import ai.boot.voice.base.BaseFragment
import ai.boot.voice.common.Const
import ai.boot.voice.model.*
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.activity_main_newsfeed_favourite.*

class MainNewsfeedFavouriteActivity : BaseFragment() {

    private lateinit var viewModel: MainNewsfeedFavouriteModel
    var articles = ArrayList<ArticleModel>()
    private lateinit var adapter: NewsfeedArticleFavouriteAdapter
    private lateinit var recycleView: RecyclerView

    var categories = ArrayList<CategoryModel>()
    private lateinit var adapterCategories: NewsfeedCategoriesAdapter
    private lateinit var recycleViewCategories: RecyclerView
//    private var nested_scroll_newsfeed_favorite = activity?.let { NestedScrollView(it) }
    private var isLoadFullData = false
    private var indexSelectedCategory = 0

    companion object {
        fun newInstance(): MainNewsfeedFavouriteActivity {
            return MainNewsfeedFavouriteActivity()
        }
    }

    override fun getRootLayoutId(): Int {
        return R.layout.activity_main_newsfeed_favourite
    }

    override fun setupViewModel() {
//        SharePreferenceUtils.getInstances().clearUserInfo()
        viewModel = ViewModelProviders.of(this).get(MainNewsfeedFavouriteModel::class.java)
        setObserveLive(viewModel)

        viewModel.callApiGetAllFavouriteArticles()
        viewModel.callApiGetAllCategories()
        adapter = NewsfeedArticleFavouriteAdapter(articles)
        adapterCategories = NewsfeedCategoriesAdapter(categories)
        viewModel()

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun setupUI(view: View) {
//
        view_next_button_next_category_favorite.visibility = View.GONE
        idPBLoading_favorite.visibility = View.GONE

        val layoutManagerArticle = object : LinearLayoutManager(activity){ override fun canScrollVertically(): Boolean{return true}}
        if (view?.findViewById<RecyclerView>(R.id.recyclerview_categories_favorite) != null) {
            recycleViewCategories =
                view?.findViewById<RecyclerView>(R.id.recyclerview_categories_favorite)!!
        }
        recycleViewCategories.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        if (view?.findViewById<RecyclerView>(R.id.recyclerview_newfeed_favorite) != null) {
            recycleView =
                view?.findViewById<RecyclerView>(R.id.recyclerview_newfeed_favorite)!!
            recycleView.isNestedScrollingEnabled = false
        }
        recycleView.layoutManager = layoutManagerArticle
////
////        // REFRESH DATA
        refresh_layout_newsfeed_favorite.setOnRefreshListener {
            refresh_layout_newsfeed_favorite.isRefreshing = false
            viewModel.reloadData(categoryId = if (indexSelectedCategory == 0) "" else categories[indexSelectedCategory].id ?: "")
        }
////
        nested_scroll_newsfeed_favorite.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (scrollY == ((v as NestedScrollView).getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) && !isLoadFullData) {
                Log.d("Load more", "newsfeed123123")
                viewModel.loadMoreData(categoryId = if (indexSelectedCategory == 0) "" else categories[indexSelectedCategory].id ?: "")
                idPBLoading_favorite.visibility = View.VISIBLE
            }
        }
//// call back
//
        next_button_newsfeed_favorite.setOnClickListener {
            loadArticleWithIndex(if (indexSelectedCategory + 1 == categories.size) 0 else indexSelectedCategory + 1)
        }

    }

    private fun viewModel() {

        viewModel.showArticles().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                idPBLoading_favorite.visibility = View.GONE
                if (it.data?.articles != null) {
                    if (viewModel.page == 1) {
                        articles = it.data?.articles ?: emptyList<ArticleModel>() as ArrayList<ArticleModel>
                    } else {
                        it.data?.articles.let { res -> articles.addAll((res.orEmpty()))}
                    }
                    reloadArticlesAdapter()
                    recycleView.adapter = adapter
                    adapter.notifyDataSetChanged()
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.showCategories().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.data?.categories != null) {
                    categories =
                        (it.data?.categories ?: emptyList<CategoryModel>()) as ArrayList<CategoryModel>
                    val allCategory = CategoryModel(id = "", name = "alle Themen")
                    categories.add(0, allCategory)
                    reloadCategoriesAdapter()
                    adapterCategories.indexSelectedCategory = indexSelectedCategory
                    next_button_newsfeed_favorite.setText(String.format("Nächstes Thema: ${if (indexSelectedCategory + 1 == categories.size) "alle Themen" else categories[indexSelectedCategory + 1].name}"))
//                    btnNext.text = String.format("Nächstes Thema: ${categories[indexSelectedCategory + 1].name}")
                    recycleViewCategories.adapter = adapterCategories
                    adapterCategories.notifyDataSetChanged()
                }

            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.noticeIdArticleSavedSuccess().observe(this, Observer { articleId ->
            val index = articles.indexOfFirst { it.id == articleId }
            if (index > -1 && index < articles.size) {
                articles[index].status = true
                adapter.notifyItemChanged(index)
            }
        })

        viewModel.noticeIdArticleUnSavedSuccess().observe(this, Observer { articleId ->
            val index = articles.indexOfFirst { it.id == articleId }
            if (index > -1 && index < articles.size) {
                articles[index].status = false
                adapter.notifyItemChanged(index)
            }
        })

        viewModel.noticeLoadFullData().observe(this, {
            isLoadFullData = it
            view_next_button_next_category_favorite.visibility = if (isLoadFullData) View.VISIBLE else View.GONE
            idPBLoading_favorite.visibility = if (isLoadFullData) View.GONE else View.VISIBLE
        })

        viewModel.noticeIdCategoryHaveNoSavedArticles().observe(this, { category ->
            categories.removeAll { it.id == category }
            indexSelectedCategory = 0
            loadArticleWithIndex(indexSelectedCategory)
        })

        adapterCategories.onItemClick = {
            loadArticleWithIndex(it)
        }

        adapter.onClickArticle = {
            var bundle = Bundle()
            bundle.putString(Const.TITLE_WEBVIEW, articles[it].title)
            bundle.putString(Const.LINK_WEBVIEW, articles[it].source)
            goToActivity(WebviewNewsfeedActivity::class.java, bundle = bundle)
        }

        adapter.onSavedArticle = {
            val article = articles[it]
            if (article.status ?: false) {
                viewModel.usSaveArticle(article.id ?: "")
            } else {
                viewModel.saveArticle(article.id ?: "", SaveArticleInput(article.categories?.first()?.id ?: ""))
            }
            articles[it].status = !(articles[it].status ?: true)
            val index = articles.indexOfFirst { it.id == article.id }
            if (index > -1 && index < articles.size) {
                adapter.notifyItemChanged(index)
            }
        }
    }

    private fun loadArticleWithIndex(index: Int) {
        val category = categories[index]
        indexSelectedCategory = index
        next_button_newsfeed_favorite.setText(String.format("Nächstes Thema: ${if (indexSelectedCategory + 1 == categories.size) "alle Themen" else categories[indexSelectedCategory + 1].name}"))

        adapterCategories.indexSelectedCategory = indexSelectedCategory
        nested_scroll_newsfeed_favorite.fullScroll(View.FOCUS_UP)
        recycleViewCategories.smoothScrollToPosition(index)
        viewModel.resetPageNumber()
        if (index == 0) {
            viewModel.callApiGetAllFavouriteArticles()
        } else {
            viewModel.callApiGetArticlesById(categoryId = category.id ?: "")
        }
        adapterCategories.notifyDataSetChanged()
    }

    private fun reloadCategoriesAdapter() {
        adapterCategories.reloadData(categories)
    }

    private fun reloadArticlesAdapter() {
        adapter.reloadData(articles)
    }
}