package ai.boot.voice.view.newfeed.category.search

import ai.boot.voice.R
import ai.boot.voice.adapter.RepositorieAdapter
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.customview.CustomBottomSheet
import ai.boot.voice.model.CategoriesModel
import ai.boot.voice.model.ItemCategoriesModel
import ai.boot.voice.view.newfeed.category.CategoryNFViewModel
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_search_category.*
import kotlinx.android.synthetic.main.activity_singin.*

class SearchCategoryActivity : BaseActivity() {

    private val itemsList: List<String> = listOf("asdf", "asdf", "asdf", "asdf")
    private var listHistoryKey:  MutableList<ItemCategoriesModel> = mutableListOf()
    private var listKeyword: MutableList<ItemCategoriesModel> = mutableListOf()
    private lateinit var adapter: SearchCategoryAdapter
    private lateinit var viewModel: SearchCategoryViewModel
    private var errorString: String = ""
    private var isSearch: Boolean = false
    private var listYourTopic: ArrayList<String> = arrayListOf()

    override fun getRootLayoutId(): Int {
        return R.layout.activity_search_category
    }

    override fun setupView(savedInstanceState: Bundle?) {
        listYourTopic = intent.getStringArrayListExtra("ListYourTopic") as ArrayList<String>

        setUI()
        actionViewModel()

        viewModel.callApiGetSearchHistory()

        btn_close.setOnClickListener {
            finish()
        }

        et_search.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            val text = et_search.text ?: ""
            if (text.isEmpty()) {
                isSearch = false
                hideKeyboard()
                return@OnEditorActionListener true
            } else {
                isSearch = true
                viewModel.callApiGetSearchKeyword(text.toString())
            }
            hideKeyboard()
            return@OnEditorActionListener true
        })

        et_search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val text = et_search.text
                iv_clear.visibility = if (text.count() > 0) { View.VISIBLE } else { View.GONE }
                if (text.count() == 0) {
                    isSearch = false
                    listHistoryKey.let { setData(it) }
                }
            }
            override fun afterTextChanged(s: Editable?) { }
        })

        iv_clear.setOnClickListener {
            et_search.setText("")
        }

        btn_clear_all.setOnClickListener {
            showBottomSheet()
        }
    }

    private fun setUI() {
        errorString = getString(R.string.message_some_thing_went_wrong)
        rl_history_search.visibility = View.GONE
    }

    private fun actionViewModel() {
        viewModel = ViewModelProviders.of(this).get(SearchCategoryViewModel::class.java)
        setObserveLive(viewModel)

        viewModel.showSearchHistory().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    it.data.let {
                        if (it != null) {
                            it.categories?.let {
                                listHistoryKey = it.toMutableList()
                                setData(it.toMutableList())
                            }
                        }
                    }
                } else {
                    makeToastSomethingWentWrong()
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.showSearchKey().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    it.data.let {
                        if (it != null) {
                            it.categories?.let {
                                listKeyword = it.toMutableList()
                                for (i in listYourTopic) {
                                    listKeyword.indexOfFirst {
                                        it.name == i
                                    }.let {
                                        if (it >= 0) {
                                            listKeyword.removeAt(it)
                                        }
                                    }
                                }
                                setData(listKeyword)
                            }
                        }
                    }
                } else {
                    makeToastSomethingWentWrong()
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.showDeleteKey().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    viewModel.callApiGetSearchHistoryNoLoading()
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.showDeleteAllKey().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    viewModel.callApiGetSearchHistoryNoLoading()
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })
    }

    private fun showBottomSheet() {
        val bottomSheet = CustomBottomSheet()
        bottomSheet.isTwoButton = true
        bottomSheet.title = "Verlauf löschen"
        bottomSheet.desc = "Sind Sie sicher, dass Sie Ihren Suchverlauf vollständig löschen möchten?"
        bottomSheet.titleButton = "Ja, löschen"
        bottomSheet.titleCancelButton = "Abbrechen"
        bottomSheet.didTouchButtonAction = {
            viewModel.callApiDeleteAllKeyHistory()
            bottomSheet.dismiss()
        }

        bottomSheet.didTouchButtonCancelAction = {
            bottomSheet.dismiss()
        }
        bottomSheet.show(supportFragmentManager, "bottomSheet")
    }

    private fun setData(model: MutableList<ItemCategoriesModel>) {
        model.let {
            if (it.count() > 0) {
                adapter = SearchCategoryAdapter(it.map {
                    if (!isSearch) { it.keyword.toString() } else { it.name.toString() }
                }, isSearch)
            }
            setChangeStatusSearch(isSearch, (it?.count() ?: 0) <= 0)
        }

        val layoutManager = LinearLayoutManager(applicationContext)
        list_recycler_view.layoutManager = layoutManager
        list_recycler_view.adapter = adapter
        adapter.notifyDataSetChanged()

        adapter.onItemClick = {
            if (isSearch) {
                val name = it
                val id = listKeyword.first {
                    it.name == name
                }?.id ?: ""
                val intent = Intent()
                intent.putExtra("KeywordSelected", it)
                intent.putExtra("IdKeywordSelected", id)
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                isSearch = true
                et_search.setText(it)
                viewModel.callApiGetSearchKeyword(it)
            }
        }

        adapter.onItemDeleteClick = {
            val name = it
            val id = listHistoryKey.first {
                it.keyword == name
            }?.id ?: ""
            if (!id.isEmpty()) {
                viewModel.callApiDeleteKeyHistory(id)
            }
        }
    }

    private fun setChangeStatusSearch(isSearch: Boolean, isNull: Boolean) {
        if (isSearch) {
            if (isNull) {
                rl_history_search.visibility = View.GONE
                ll_result_empty.visibility = View.VISIBLE
            } else {
                rl_history_search.visibility = View.VISIBLE
                ll_result_empty.visibility = View.GONE
                tv_title_result.setText("Suchergebnisse")
                btn_clear_all.visibility = View.GONE
            }
        } else {
            if (isNull) { rl_history_search.visibility = View.GONE } else {
                rl_history_search.visibility = View.VISIBLE
                ll_result_empty.visibility = View.VISIBLE
                tv_title_result.setText("Zuletzt gesucht")
                btn_clear_all.visibility = View.VISIBLE
            }
        }
    }
}