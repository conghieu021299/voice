package ai.boot.voice.view.newfeed.category

import ai.boot.voice.api.ApiBuilder
import ai.boot.voice.api.MessageModel
import ai.boot.voice.api.MessageResponse
import ai.boot.voice.api.ResponseModel
import ai.boot.voice.base.BaseViewModel
import ai.boot.voice.model.CategoryNFModel
import ai.boot.voice.model.InputKeyword
import ai.boot.voice.model.InputStartNewfeed
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CategoryNFViewModel: BaseViewModel() {
    private val categoriesResponse = MutableLiveData<ResponseModel<CategoryNFModel>>()
    private val startNewFeedResponse = MutableLiveData<MessageResponse>()

    fun showCategories(): MutableLiveData<ResponseModel<CategoryNFModel>> {
        return categoriesResponse
    }

    fun showStartNewfeed(): MutableLiveData<MessageResponse> {
        return startNewFeedResponse
    }

    fun callApiGetCategory() {
        disposables.add(
            ApiBuilder.getWebService().getCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    categoriesResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<CategoryNFModel>(message = message)
                    categoriesResponse.value = response
                })
        )
    }

    fun callApiSaveKeyword(key: String) {
        val model = InputKeyword(key)
        disposables.add(
            ApiBuilder.getWebService().postSaveKeyword(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        )
    }

    fun callApiStartNewfeed(key: InputStartNewfeed) {
        disposables.add(
            ApiBuilder.getWebService().postStartNewfeed(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    startNewFeedResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = MessageResponse(message = message)
                    startNewFeedResponse.value = response
                })
        )
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}
