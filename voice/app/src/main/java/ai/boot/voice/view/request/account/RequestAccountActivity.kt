package ai.boot.voice.view.request.account

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.common.extensions.capitalizeFirstLetter
import ai.boot.voice.common.extensions.isEmailValid
import ai.boot.voice.common.extensions.isNameValid
import ai.boot.voice.common.extensions.removeAllSpace
import ai.boot.voice.customview.CustomBottomSheet
import ai.boot.voice.model.InputSignup
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.forgot.password.ForgotPasswordViewModel
import ai.boot.voice.view.reset.password.ResetPasswordActivity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.et_email
import kotlinx.android.synthetic.main.activity_request_account.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class RequestAccountActivity : BaseActivity() {
    private lateinit var viewModel: RequestAccountViewModel
    private var errorString: String = ""

    override fun getRootLayoutId(): Int {
        return R.layout.activity_request_account
    }

    override fun setupView(savedInstanceState: Bundle?) {
        errorString = getString(R.string.message_some_thing_went_wrong)

        viewModel = ViewModelProviders.of(this).get(RequestAccountViewModel::class.java)
        setObserveLive(viewModel)

        viewModel.showUserInfo().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    showBottomSheet(
                        "Information",
                        it.message?.text_de.toString(),
                        "Verstanden",
                        callback = {
                            finish()
                        })
                } else {
                    showBottomSheet(
                        "Anfrage fehlgeschlagen",
                        it.message?.text_de.toString(),
                        "Zurück zu Anfrage",
                    isCancel = true)
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        navigation_request_account.setOnClickListener {
            finish()
        }

        btn_request_account.setOnClickListener {
            if (isValidEmail()) {
                val email = et_email.text.toString().trim()
                val firstname = et_firstname.text.toString()
                val lastname = et_lastname.text.toString()
                viewModel.callApiSignup(InputSignup(email, firstname, lastname))
            }
        }

        actionEditText()
        actionFocusEditText()
        actionKeyboardReturn()
    }

    private fun actionEditText() {
        et_email.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                isEnableButttonLogin()
            }
        })

        et_firstname.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                et_firstname.removeTextChangedListener(this)
                isEnableButttonLogin()
                et_firstname.setText(et_firstname.text.toString().capitalizeFirstLetter())
                et_firstname.setSelection(s?.length ?: 0)
                et_firstname.addTextChangedListener(this)
            }
        })

        et_lastname.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                et_lastname.removeTextChangedListener(this)
                isEnableButttonLogin()
                et_lastname.setText(et_lastname.text.toString().capitalizeFirstLetter())
                et_lastname.setSelection(s?.length ?: 0)
                et_lastname.addTextChangedListener(this)
            }
        })
    }

    private fun actionFocusEditText() {
        et_email.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                et_email.setBackgroundResource(R.drawable.custom_input_login)
                tv_email_address_error.visibility = View.GONE
                iv_ic_email.visibility = View.GONE
            }
        }

        et_firstname.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                et_firstname.setBackgroundResource(R.drawable.custom_input_login)
                tv_firstname_error.visibility = View.GONE
                iv_ic_firstname.visibility = View.GONE
            }
        }

        et_lastname.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                et_lastname.setBackgroundResource(R.drawable.custom_input_login)
                tv_lastname_error.visibility = View.GONE
                iv_ic_lastname.visibility = View.GONE
            }
        }
    }

    private fun actionKeyboardReturn() {
        et_email.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            isValidEmail()
            hideKeyboard()
            return@OnEditorActionListener true
        })

        et_firstname.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            isValidName(et_firstname)
            hideKeyboard()
            return@OnEditorActionListener true
        })

        et_lastname.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            isValidName(et_lastname)
            hideKeyboard()
            return@OnEditorActionListener true
        })
    }

    private fun isValidEmail(): Boolean {
        val email = et_email.text.toString().trim()
        iv_ic_email.visibility = View.VISIBLE
        if (email.count() < 3 || !email.isEmailValid()) {
            tv_email_address_error.visibility = View.VISIBLE
            tv_email_address_error.text = getString(R.string.message_1)
            et_email.setBackgroundResource(R.drawable.input_login_error)
            iv_ic_email.setImageResource(R.drawable.ic_x_red)
            return false
        } else {
            tv_email_address_error.visibility = View.GONE
            et_email.setBackgroundResource(R.drawable.custom_input_login)
            iv_ic_email.setImageResource(R.drawable.ic_check_white)
            return true
        }
    }

    private fun isValidName(et: EditText): Boolean {
        val text = et.text.toString().trim()
        if (text.count() < 1 || !text.isNameValid()) {
            et.setBackgroundResource(R.drawable.input_login_error)
            return false
        } else {
            et.setBackgroundResource(R.drawable.custom_input_login)
            return true
        }
    }

    private fun isEnableButttonLogin() {
        val email = et_email.text.toString().trim()
        val firstname = et_firstname.text.toString()
        val lastname = et_lastname.text.toString()
        var isCheckSpaceFirstname: Boolean = false
        var isCheckSpaceLastname: Boolean = false
        if (firstname.removeAllSpace().count() > 0) {
            isCheckSpaceFirstname = true
        }

        if (lastname.removeAllSpace().count() > 0) {
            isCheckSpaceLastname = true
        }

        btn_request_account.isEnabled = email.isEmailValid() && firstname.isNameValid() && lastname.isNameValid() && isCheckSpaceFirstname && isCheckSpaceLastname
    }

    private fun showBottomSheet(title: String, desc: String? = errorString, titleButton: String, isCancel: Boolean? = null, callback: (() -> Unit)? = null) {
        val bottomSheet = CustomBottomSheet()
        bottomSheet.title = title
        bottomSheet.desc = desc.toString()
        bottomSheet.titleButton = titleButton
        bottomSheet.didTouchButtonAction = {
            bottomSheet.dismiss()
            callback?.invoke()
        }
        bottomSheet.isCancelable = isCancel ?: false
        bottomSheet.show(supportFragmentManager, "bottomSheet")
    }
}