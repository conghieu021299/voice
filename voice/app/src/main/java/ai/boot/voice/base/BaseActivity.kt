package ai.boot.voice.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ai.boot.voice.R
import ai.boot.voice.common.Const
import ai.boot.voice.common.DialogUtils
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.MainActivity
import ai.boot.voice.view.newfeed.category.CategoryNFActivity
import ai.boot.voice.view.newsfeed.MainNewsfeedActivity
import ai.boot.voice.view.signin.SinginActivity
import ai.boot.voice.view.wellcome.WellcomeActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.view_header.*

abstract class BaseActivity : AppCompatActivity() {
    private var isBackPressed: Boolean = false
    protected val TAG = BaseActivity::class.java.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.anim_right_to_left_enter, R.anim.anim_right_to_left_leave)
        setContentView(getRootLayoutId())
        setupView(savedInstanceState)
    }

    abstract fun getRootLayoutId(): Int

    abstract fun setupView(savedInstanceState: Bundle?)

    fun setTitleToolbar(title: String? = "") {
        tv_title.text = title
        ln_left.setOnClickListener {
            onBackPressed()
        }
    }

    fun goToActivity(c: Class<*>, activityClearTop: Boolean? = false, bundle: Bundle? = null) {
        val intent = Intent(this, c)
        bundle?.let {
            intent.putExtra(Const.KEY_EXTRA_DATA, bundle)
        }

        if (activityClearTop == true) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }

        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    fun checkLoginFlow() {
        val idToken = SharePreferenceUtils.getInstances().getIdToken()
        val listEmail = SharePreferenceUtils.getInstances().getListEmailLogin()?.email_list
        if (idToken != null && idToken != "") {
            val indexCheck = listEmail?.indexOfFirst {
                it == SharePreferenceUtils.getInstances().getEmail()
            } ?: -1
            if (indexCheck == -1) {
                SharePreferenceUtils.getInstances().saveWellcomeFirstTime(false)
                SharePreferenceUtils.getInstances().saveCategoryFirstTime(false)
                goToActivity(WellcomeActivity::class.java)
                finish()
            } else {
                if (SharePreferenceUtils.getInstances().getWellcomeFirstTime() == false) {
                    goToActivity(WellcomeActivity::class.java)
                    finish()
                } else if (SharePreferenceUtils.getInstances().getCategoryFirstTime() == false) {
                    goToActivity(CategoryNFActivity::class.java)
                    finish()
                } else {
                    goToActivity(MainActivity::class.java)
                    finish()
                }
            }
        } else {
            goToActivity(SinginActivity::class.java)
            finish()
        }
    }

    fun extraData(getData: (Bundle) -> Unit) {
        var bundle = intent.getBundleExtra(Const.KEY_EXTRA_DATA)
        if (bundle != null) {
            getData.invoke(bundle)
        }
    }

    fun makeToastSomethingWentWrong() {
        Toast.makeText(this, getString(R.string.message_some_thing_went_wrong), Toast.LENGTH_SHORT)
            .show()
    }

    fun makeToastWithMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun replaceFragment(viewID: Int, fragment: Fragment?, tag: String? = null) {
        var transaction = supportFragmentManager.beginTransaction()
        transaction.add(viewID, fragment!!)
        transaction.addToBackStack(tag)
        transaction.commit()
    }

    fun setObserveLive(viewModel: BaseViewModel) {
        viewModel.eventLoading.observe(this, Observer {
            if (it != null) {
                if (it.getContentIfNotHandled() != null) {
                    if (it.peekContent()) {
                        showLoadingDialog()
                    } else {
                        hideLoadingDialog()
                    }
                }
            }
        })
        viewModel.eventFailure.observe(this, Observer {
            if (it != null) {
                if (it.getContentIfNotHandled() != null) {
                    showFailure(it.peekContent())
                }
            }
        })

    }

    fun showLoadingDialog() {
        DialogUtils.showLoadingDialog(this)
    }

    fun hideLoadingDialog() {
        DialogUtils.dismiss()
    }

    private fun showFailure(throwable: Throwable) {
        if (throwable.message != null) {
            Log.i(TAG, "showQuestFailure: " + throwable.message)
        }
    }

    fun widthItem(number: Int): Int {
        return Resources.getSystem().displayMetrics.widthPixels / number
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onBackPressed() {
        if (!isTaskRoot) {
            super.onBackPressed()
            overridePendingTransition(
                R.anim.anim_left_to_right_enter,
                R.anim.anim_left_to_right_leave
            )
            return
        }
        if (isBackPressed) {
            super.onBackPressed()
            overridePendingTransition(
                R.anim.anim_left_to_right_enter,
                R.anim.anim_left_to_right_leave
            )
        } else {
            Toast.makeText(this, R.string.back_again_to_exit, Toast.LENGTH_SHORT).show()
            isBackPressed = true
            Handler().postDelayed({ isBackPressed = false }, 2000)
        }
    }
}