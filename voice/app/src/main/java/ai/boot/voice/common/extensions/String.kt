package ai.boot.voice.common.extensions

import android.annotation.SuppressLint
import java.io.UnsupportedEncodingException
import java.lang.Byte.decode
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

fun String.encodeSH256(): String {
    var md: MessageDigest? = null
    var oauthToken = ""
    try {
        md = MessageDigest.getInstance("SHA-256")
        md!!.update(this.toByteArray(charset("UTF-8")))
        val digest = md.digest()
        oauthToken = String.format("%064x", java.math.BigInteger(1, digest))
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    } catch (e: UnsupportedEncodingException) {
        e.printStackTrace()
    }

    return oauthToken
}

fun String.isEmailValid(): Boolean {
    val regex = "[A-Z0-9a-z._%+\\-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(this)
    return matcher.matches()

}

fun String.isNameValid(): Boolean {
    val regex = "[A-Za-z-äöüÄÖÜùûüÿàâæéèêëïîôœÙÛÜŸÀÂÆÉÈÊËÏÎÔŒß'‘ ]{1,50}"
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(this)
    return matcher.matches()
}

fun String.isInputEmailValid(): Boolean {
    val regex = "[A-Z0-9a-z._%+\\-]+"
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(this)
    return matcher.matches()

}

fun String.isValidPassword(): Boolean {
    val regex = "^(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9!@#\$%^&*()\\\\-_=+{}|?>.<,:;]{8,16}\$"
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(this)
    return matcher.matches()
}

fun String.isValidMobile(): Boolean {
    return if (!Pattern.matches("[a-zA-Z]+", this)) {
        this.length in 7..13
    } else false
}



fun String.formatPrice(currentCy : String): String {
    var price = this.toDouble()
    val formatter: NumberFormat = DecimalFormat("#,###")
    return formatter.format(price) + " $currentCy"
}

fun String.capitalizeFirstLetter(): String {
    val capBuffer = StringBuffer()
    val capMatcher: Matcher =
        Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(this)
    while (capMatcher.find()) {
        capMatcher.appendReplacement(
            capBuffer,
            capMatcher.group(1).uppercase() + capMatcher.group(2)
        )
    }
    return capMatcher.appendTail(capBuffer).toString()
}

fun String.removeAllSpace(): String {
    return this.replace(" ", "")
}

fun String.convertToDate(formatLocal: String): Date? {
    val format = SimpleDateFormat(formatLocal)
    try {
        return format.parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return null
}

fun String.changeFormatDate(fromFormat: String, toFormat: String): String? {
    val dateThis = this.convertToDate(fromFormat)
    val dateFormat = SimpleDateFormat(toFormat)
    try {
        return dateFormat.format(dateThis)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return null
}