package ai.boot.voice.model

data class SigninResponse (
    var last_name: String? = null,
    var status: String? = null,
    var first_name: String? = null,
    var avatar: String? = null,
    var gender: Int? = null,
    var group_id: String? = null,
    var email: String? = null,
    var birth_date: String? = null,
    var id: String? = null,
    var id_token: String? = null,
    var access_token: String? = null,
    var refresh_token: String? = null,
    var challengename: String? = null,
    var session: String? = null
)