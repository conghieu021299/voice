package ai.boot.voice.model

data class InputRefreshToken(
    var refresh_token: String
)

data class InputLoginModel(
    var email: String,
    var password: String
)

data class InputForceChangePassword(
    var email: String,
    var new_password: String,
    var session: String
)

data class InputForgotPassword(
    var email: String,
    var new_password: String,
    var activate_code: String
)

data class InputKeyword(
    var keyword: String,
)

data class InputStartNewfeed(
    var keywords: MutableList<String> = mutableListOf()
)

data class InputSignup(
    var email: String,
    var first_name: String,
    var last_name: String
)

data class SaveListEmailLogin(
    var email_list: MutableList<String> = mutableListOf()
)

data class InputEditProfile(
    var gender: Int,
    var first_name: String,
    var last_name: String,
    var birth_date: String,
    var mobile: String
)

