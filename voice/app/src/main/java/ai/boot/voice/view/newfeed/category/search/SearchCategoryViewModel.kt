package ai.boot.voice.view.newfeed.category.search

import ai.boot.voice.api.ApiBuilder
import ai.boot.voice.api.MessageModel
import ai.boot.voice.api.MessageResponse
import ai.boot.voice.api.ResponseModel
import ai.boot.voice.base.BaseViewModel
import ai.boot.voice.model.CategoriesModel
import ai.boot.voice.model.CategoryNFModel
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SearchCategoryViewModel: BaseViewModel() {
    private val historyResponse = MutableLiveData<ResponseModel<CategoriesModel>>()
    private val keyResponse = MutableLiveData<ResponseModel<CategoriesModel>>()
    private val deleteKeyResponse = MutableLiveData<MessageResponse>()
    private val deleteAllKeyResponse = MutableLiveData<MessageResponse>()

    fun showSearchHistory(): MutableLiveData<ResponseModel<CategoriesModel>> {
        return historyResponse
    }

    fun showSearchKey(): MutableLiveData<ResponseModel<CategoriesModel>> {
        return keyResponse
    }

    fun showDeleteKey(): MutableLiveData<MessageResponse> {
        return deleteKeyResponse
    }

    fun showDeleteAllKey(): MutableLiveData<MessageResponse> {
        return deleteAllKeyResponse
    }

    fun callApiGetSearchHistory() {
        disposables.add(
            ApiBuilder.getWebService().getSearchHistoryCategory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    historyResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<CategoriesModel>(message = message)
                    historyResponse.value = response
                })
        )
    }

    fun callApiGetSearchKeyword(key: String) {
        disposables.add(
            ApiBuilder.getWebService().getSearchKeyword(key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    keyResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<CategoriesModel>(message = message)
                    keyResponse.value = response
                })
        )
    }

    fun callApiDeleteKeyHistory(id: String) {
        disposables.add(
            ApiBuilder.getWebService().deleteKeywordHistory(id)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { showLoading(true) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    deleteKeyResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = MessageResponse(message = message)
                    deleteKeyResponse.value = response
                })
        )
    }

    fun callApiGetSearchHistoryNoLoading() {
        disposables.add(
            ApiBuilder.getWebService().getSearchHistoryCategory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { showLoading(false) }
                .subscribe({
                    historyResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<CategoriesModel>(message = message)
                    historyResponse.value = response
                })
        )
    }

    fun callApiDeleteAllKeyHistory() {
        disposables.add(
            ApiBuilder.getWebService().deleteAllKeywordHistory()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { showLoading(true) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    deleteAllKeyResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = MessageResponse(message = message)
                    deleteAllKeyResponse.value = response
                })
        )
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}