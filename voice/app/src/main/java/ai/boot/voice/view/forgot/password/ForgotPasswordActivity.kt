package ai.boot.voice.view.forgot.password

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.common.extensions.isEmailValid
import ai.boot.voice.customview.CustomBottomSheet
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.reset.password.ResetPasswordActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.navigation_view
import kotlinx.android.synthetic.main.activity_reset_password.*

class ForgotPasswordActivity : BaseActivity() {
    private lateinit var viewModel: ForgotPasswordViewModel
    private var email: String = ""
    private var errorString: String = ""

    override fun getRootLayoutId(): Int {
        return R.layout.activity_forgot_password
    }

    override fun setupView(savedInstanceState: Bundle?) {
        errorString = getString(R.string.message_some_thing_went_wrong)

        viewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel::class.java)
        setObserveLive(viewModel)

        viewModel.showUserInfo().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    showBottomSheet(
                        "Information",
                        it.message?.text_de,
                        "Verstanden"
                    ) {
                        SharePreferenceUtils.getInstances().saveEmail(et_email.text.toString().trim())
                        val bundle = Bundle()
                        bundle.putBoolean(Const.FORGOT_PASSWORD, true)
                        goToActivity(ResetPasswordActivity::class.java, false, bundle)
                    }
                } else {
                    showBottomSheet(
                        "Information",
                        it.message?.text_de,
                        "Verstanden"
                    )
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        et_email.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                isEnableButttonLogin()
            }
        })

        et_email.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                et_email.setBackgroundResource(R.drawable.custom_input_login)
                tv_email_error.visibility = View.GONE
                iv_icon_email.visibility = View.GONE
            }
        }

        et_email.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            isValidUsername()
            hideKeyboard()
            return@OnEditorActionListener true
        })

        btn_forgot_password.setOnClickListener {
            if (isValidUsername()) {
                viewModel.callApiForgotPassword(et_email.text.toString().trim())
            }
        }

        navigation_view.setOnClickListener {
            finish()
        }
    }

    private fun isEnableButttonLogin() {
        email = et_email.text.toString().trim()
        btn_forgot_password.isEnabled = email.isEmailValid()
    }

    private fun isValidUsername(): Boolean {
        email = et_email.text.toString().trim()
        iv_icon_email.visibility = View.VISIBLE
        if (email.count() < 3 || !email.isEmailValid()) {
            tv_email_error.visibility = View.VISIBLE
            tv_email_error.text = getString(R.string.message_1)
            et_email.setBackgroundResource(R.drawable.input_login_error)
            iv_icon_email.setImageResource(R.drawable.ic_x_red)
            return false
        } else {
            tv_email_error.visibility = View.GONE
            et_email.setBackgroundResource(R.drawable.custom_input_login)
            iv_icon_email.setImageResource(R.drawable.ic_check_white)
            return true
        }
    }

    private fun showBottomSheet(title: String, desc: String? = errorString, titleButton: String, callback: (() -> Unit)? = null) {
        val bottomSheet = CustomBottomSheet()
        bottomSheet.title = title
        bottomSheet.desc = desc.toString()
        bottomSheet.titleButton = titleButton
        bottomSheet.didTouchButtonAction = {
            callback?.invoke()
            bottomSheet.dismiss()
        }
        bottomSheet.isCancelable = false
        bottomSheet.show(supportFragmentManager, "bottomSheet")
    }

    override fun onBackPressed() {
        finish()
    }
}