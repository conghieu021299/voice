package ai.boot.voice.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import ai.boot.voice.MyApplication
import ai.boot.voice.model.UserEntity
import ai.boot.voice.common.Const
import ai.boot.voice.model.SaveListEmailLogin
import ai.boot.voice.model.SigninResponse
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class SharePreferenceUtils {
    val name = "BASEKOTLIN"

    private constructor() {
        mPrefs = MyApplication.context?.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    companion object {
        private var mPrefs: SharedPreferences? = null
        private var instance: SharePreferenceUtils = SharePreferenceUtils()
        fun getInstances(): SharePreferenceUtils {
            if (instance == null) {
                instance = SharePreferenceUtils()
            }
            return instance
        }
    }

    fun saveString(key: String, content: String?) {
        mPrefs?.edit()?.putString(key, content)?.apply()
    }

    fun removeString(key: String) {
        mPrefs?.edit()?.remove(key)?.apply()
    }

    fun getString(key: String): String? {
        return mPrefs?.getString(key, "")
    }

    fun saveBoolean(key: String, content: Boolean) {
        mPrefs?.edit()?.putBoolean(key, content)?.apply()
    }

    fun removeBoolean(key: String) {
        mPrefs?.edit()?.remove(key)?.apply()
    }

    fun getBoolean(key: String): Boolean? {
        return mPrefs?.getBoolean(key, false)
    }

    fun saveInt(key: String, content: Int?) {
        content?.let {
            mPrefs?.edit()?.putInt(key, content)?.apply()
        }
    }

    fun getInt(key: String): Int {
        return mPrefs?.getInt(key, -1) ?: -1
    }

    fun saveUserInfor(userInfor: SigninResponse?) {
        val gson = Gson()
        val json = gson.toJson(userInfor)
        mPrefs?.edit()?.putString(Const.KEY_USER, json)?.apply()
    }

    fun getUserInfor(): SigninResponse? {
        try {
            val json = mPrefs?.getString(Const.KEY_USER, null) ?: return null
            return Gson().fromJson(json, SigninResponse::class.java)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return null
    }

    fun saveIdToken(token: String) {
        mPrefs?.edit()?.putString("saveIdToken", token)?.apply()
    }

    fun getIdToken(): String? {
        return mPrefs?.getString("saveIdToken", "")
    }

    fun saveRefreshToken(token: String) {
        mPrefs?.edit()?.putString("saveRefreshToken", token)?.apply()
    }

    fun getRefreshToken(): String? {
        return mPrefs?.getString("saveRefreshToken", "")
    }

    fun saveSession(token: String) {
        mPrefs?.edit()?.putString("saveSession", token)?.apply()
    }

    fun getSession(): String? {
        return mPrefs?.getString("saveSession", "")
    }

    fun saveEmail(token: String) {
        mPrefs?.edit()?.putString("saveEmail", token)?.apply()
    }

    fun getEmail(): String? {
        return mPrefs?.getString("saveEmail", "")
    }

    fun saveWellcomeFirstTime(token: Boolean) {
        mPrefs?.edit()?.putBoolean("saveWellcomeFirstTime", token)?.apply()
    }

    fun getWellcomeFirstTime(): Boolean? {
        return mPrefs?.getBoolean("saveWellcomeFirstTime", false)
    }

    fun saveCategoryFirstTime(token: Boolean) {
        mPrefs?.edit()?.putBoolean("saveCategoryFirstTime", token)?.apply()
    }

    fun getCategoryFirstTime(): Boolean? {
        return mPrefs?.getBoolean("saveCategoryFirstTime", false)
    }

    fun saveListEmailLogin(email: String) {
        if (email.isEmpty()) { return }
        val emailList: MutableList<String> = mutableListOf()
        getListEmailLogin()?.email_list?.let { emailList.addAll(it) }
        emailList.add(email)
        val gson = Gson()
        val json = gson.toJson(SaveListEmailLogin(emailList))
        mPrefs?.edit()?.putString(Const.KEY_EMAIL_LIST, json)?.apply()
    }

    fun getListEmailLogin(): SaveListEmailLogin? {
        try {
            val json = mPrefs?.getString(Const.KEY_EMAIL_LIST, null) ?: return null
            return Gson().fromJson(json, SaveListEmailLogin::class.java)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return null
    }

    fun clearUserInfo() {
        removeString(Const.KEY_USER)
        saveIdToken("")
    }
}