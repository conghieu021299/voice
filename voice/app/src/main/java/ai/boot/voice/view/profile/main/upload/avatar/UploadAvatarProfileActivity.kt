package ai.boot.voice.view.profile.main.upload.avatar

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.common.extensions.CircleTransform
import ai.boot.voice.view.newfeed.category.CategoryNFViewModel
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_upload_avatar_profile.*

class UploadAvatarProfileActivity : BaseActivity() {

    var uriImage: Uri? = null
    private lateinit var viewModel: UploadAvatarProfileViewModel

    override fun getRootLayoutId(): Int {
        return R.layout.activity_upload_avatar_profile
    }

    override fun setupView(savedInstanceState: Bundle?) {
        extraData {
            uriImage = Uri.parse(it.getString("UriImage"))
        }

        viewModel = ViewModelProviders.of(this).get(UploadAvatarProfileViewModel::class.java)
        setObserveLive(viewModel)

        Picasso.get().load(uriImage)
            .resize(200, 200)
            .placeholder(R.mipmap.avatar_placeholder)
            .error(R.mipmap.avatar_placeholder)
            .transform(CircleTransform())
            .centerCrop().into(iv_avatar)

        iv_back.setOnClickListener {
            finish()
        }

        btn_save_avatar.setOnClickListener {
            uriImage?.let { it1 -> viewModel.callApiGetLinkUpload(it1) }
        }
    }
}