package ai.boot.voice.api

import ai.boot.voice.MyApplication
import ai.boot.voice.utils.SharePreferenceUtils
import okhttp3.Interceptor
import okhttp3.Response
import ai.boot.voice.common.Const
import ai.boot.voice.model.InputRefreshToken
import ai.boot.voice.view.signin.SinginActivity
import android.content.Intent
import kotlinx.coroutines.runBlocking
import java.io.IOException


class APIInterceptor(private val retrofitBuilder: ApiBuilder.Companion) : Interceptor {
    @Throws(IOException::class)

    override fun intercept(chain: Interceptor.Chain): Response {
        synchronized(this) {
            val inputRefreshToken: String = SharePreferenceUtils.getInstances().getRefreshToken() ?: ""
            val originalRequest = chain.request()
            val authenticationRequest = originalRequest.newBuilder()
                .addHeader("Authorization", inputRefreshToken)
                .addHeader("Content-Type", "application/json").build()
            val initialResponse = chain.proceed(authenticationRequest)

            when {
                initialResponse.code == 403 -> {
                    //RUN BLOCKING!!
                    val responseNewTokenLoginModel = runBlocking {
                        retrofitBuilder.getWebService()
                            .postRefreshToken(InputRefreshToken(inputRefreshToken)).execute()
                    }

                    return when {
                        responseNewTokenLoginModel == null || responseNewTokenLoginModel.code() != 200 || responseNewTokenLoginModel.body()?.message?.status != Const.SUCCESS -> {
                            SharePreferenceUtils.getInstances().clearUserInfo()
                            val context = MyApplication.context
                            val intent = Intent(context, SinginActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            context?.startActivity(intent)
                            return initialResponse
                        }
                        else -> {
                            initialResponse.close()
                            responseNewTokenLoginModel.body()?.data?.id_token.let {
                                SharePreferenceUtils.getInstances().saveIdToken(it ?: "")
                            }
                            val newAuthenticationRequest = originalRequest.newBuilder().addHeader(
                                "Authorization",
                                responseNewTokenLoginModel.body()?.data?.id_token ?: ""
                            ).build()
                            chain.proceed(newAuthenticationRequest)
                        }
                    }
                }
                else -> return initialResponse
            }
        }
    }
}