package ai.boot.voice.view.reset.password

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import ai.boot.voice.common.extensions.isValidPassword
import ai.boot.voice.common.extensions.removeSpaceChar
import ai.boot.voice.customview.CustomBottomSheet
import ai.boot.voice.model.InputForceChangePassword
import ai.boot.voice.model.InputForgotPassword
import ai.boot.voice.model.SigninResponse
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.view.MainActivity
import ai.boot.voice.view.signin.SinginActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import kotlinx.android.synthetic.main.activity_reset_password.*
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_reset_password.navigation_view


class ResetPasswordActivity : BaseActivity() {
    private lateinit var viewModel: ResetPasswordViewModel
    private var isShowNewPassword: Boolean = false
    private var isShowConfirmPassword: Boolean = false
    private var errorString: String = ""
    private var isForgotPasswordView: Boolean = false
    private var verification: String = ""
    private var newPassword: String = ""
    private var confirmPassword: String = ""

    override fun getRootLayoutId(): Int {
        return R.layout.activity_reset_password
    }

    override fun setupView(savedInstanceState: Bundle?) {
        extraData {
            isForgotPasswordView = it.getBoolean(Const.FORGOT_PASSWORD)
        }

        if (!isForgotPasswordView) {
            et_verification.visibility = View.GONE
            iv_ic_verifition.visibility = View.GONE
            tv_verification_error.visibility = View.GONE
        }

        viewModelAction()

        errorString = getString(R.string.message_some_thing_went_wrong)
        setOnFocus(et_verification, tv_verification_error)
        setOnFocus(et_new_password, tv_new_password_error)
        setOnFocus(et_accept_password, tv_accept_password_error)

        iv_ic_new_password.setOnClickListener {
            if (isShowNewPassword) {
                iv_ic_new_password.setImageResource(R.drawable.ic_eye_show)
                et_new_password.transformationMethod = PasswordTransformationMethod.getInstance()
            } else {
                iv_ic_new_password.setImageResource(R.drawable.ic_eye_slash)
                et_new_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }
            isShowNewPassword = !isShowNewPassword
        }

        iv_ic_accept_password.setOnClickListener {
            if (isShowConfirmPassword) {
                iv_ic_accept_password.setImageResource(R.drawable.ic_eye_show)
                et_accept_password.transformationMethod = PasswordTransformationMethod.getInstance()
            } else {
                iv_ic_accept_password.setImageResource(R.drawable.ic_eye_slash)
                et_accept_password.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            }
            isShowConfirmPassword = !isShowConfirmPassword
        }

        et_verification.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            isValidVerification()
            hideKeyboard()
            return@OnEditorActionListener true
        })

        et_new_password.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            isValidNewPassword()
            hideKeyboard()
            return@OnEditorActionListener true
        })

        et_accept_password.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            isValidConfirmPassword()
            hideKeyboard()
            return@OnEditorActionListener true
        })

        btn_reset_password.setOnClickListener {
            if (isForgotPasswordView) {
                if (isValidVerification() && isValidNewPassword() && isValidConfirmPassword()) {
                    newPassword = et_new_password.text.toString().trim()
                    verification = et_verification.text.toString().trim()
                    val email: String =
                        SharePreferenceUtils.getInstances().getEmail().toString()
                    viewModel.callApiConfirmNewPassword(InputForgotPassword(email, newPassword, verification))
                }
            } else {
                if (isValidNewPassword() && isValidConfirmPassword()) {
                    newPassword = et_new_password.text.toString().trim()
                    val email: String =
                        SharePreferenceUtils.getInstances().getEmail().toString()
                    val session: String = SharePreferenceUtils.getInstances().getSession().toString()
                    viewModel.callApiForceChangePassword(InputForceChangePassword(email, newPassword, session))
                }
            }
        }

        navigation_view.setOnClickListener {
            finish()
        }

        addTextChanged(et_verification)
        addTextChanged(et_new_password)
        addTextChanged(et_accept_password)
        et_new_password.removeSpaceChar(16)
        et_accept_password.removeSpaceChar(16)
    }

    private fun addTextChanged(et: EditText) {
        et.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                isEnableButttonLogin()
            }
        })
    }

    private fun setOnFocus(et: EditText, tv: TextView) {
        et.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                et.setBackgroundResource(R.drawable.custom_input_login)
                tv.visibility = View.GONE
                if (et == et_verification) {
                    iv_ic_verifition.visibility = View.GONE
                }
            }
        }
    }

    private fun isEnableButttonLogin() {
        verification = et_verification.text.toString().trim()
        newPassword = et_new_password.text.toString().trim()
        confirmPassword = et_accept_password.text.toString().trim()
        if (isForgotPasswordView) {
            btn_reset_password.isEnabled =
                verification.isNotEmpty() && newPassword.isNotEmpty() && confirmPassword.isNotEmpty()
        } else {
            btn_reset_password.isEnabled = newPassword.isNotEmpty() && confirmPassword.isNotEmpty()
        }
    }

    private fun isValidVerification(): Boolean {
        verification = et_verification.text.toString().trim()
        if (verification.count() < 1) {
            tv_verification_error.visibility = View.VISIBLE
            tv_verification_error.text = getString(R.string.message_21)
            et_verification.setBackgroundResource(R.drawable.input_login_error)
            return false
        } else {
            tv_verification_error.visibility = View.GONE
            et_verification.setBackgroundResource(R.drawable.custom_input_login)
            return true
        }
    }

    private fun isValidNewPassword(): Boolean {
        newPassword = et_new_password.text.toString().trim()
        if (newPassword.count() < 8 || !newPassword.isValidPassword()) {
            tv_new_password_error.visibility = View.VISIBLE
            tv_new_password_error.text = getString(R.string.message_3)
            et_new_password.setBackgroundResource(R.drawable.input_login_error)
            return false
        } else {
            tv_new_password_error.visibility = View.GONE
            et_new_password.setBackgroundResource(R.drawable.custom_input_login)
            return true
        }
    }

    private fun isValidConfirmPassword(): Boolean {
        confirmPassword = et_accept_password.text.toString().trim()
        newPassword = et_new_password.text.toString().trim()
        if (confirmPassword.count() < 8 || !confirmPassword.isValidPassword()) {
            tv_accept_password_error.visibility = View.VISIBLE
            tv_accept_password_error.text = getString(R.string.message_3)
            et_accept_password.setBackgroundResource(R.drawable.input_login_error)
            return false
        } else if (confirmPassword != newPassword) {
            tv_accept_password_error.visibility = View.VISIBLE
            tv_accept_password_error.text = getString(R.string.password_not_mathch)
            et_accept_password.setBackgroundResource(R.drawable.input_login_error)
            return false
        } else {
            tv_accept_password_error.visibility = View.GONE
            et_accept_password.setBackgroundResource(R.drawable.custom_input_login)
            return true
        }
    }

    private fun setForceChangePassword(it: SigninResponse?) {
        if (it != null) {
            it.id_token?.let { it1 -> SharePreferenceUtils.getInstances().saveIdToken(it1) }
            it.refresh_token?.let { it1 -> SharePreferenceUtils.getInstances().saveRefreshToken(it1) }
            SharePreferenceUtils.getInstances().saveUserInfor(it)
            checkLoginFlow()
            finish()
        }
    }

    private fun viewModelAction() {
        viewModel = ViewModelProviders.of(this).get(ResetPasswordViewModel::class.java)
        setObserveLive(viewModel)

        viewModel.showForceChangePassword().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    if (it.data != null) {
                        setForceChangePassword(it.data)
                    } else {
                        showBottomSheet(
                            "Passwort erfolgreich geändert",
                            it.message?.text_de,
                            "Zum Login"
                        )
                    }
                } else {
                    showBottomSheet(
                        "Zurücksetzen fehlgeschlagen",
                        it.message?.text_de,
                        "Verstanden"
                    )
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.showConfirmNewPassword().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    showBottomSheet(
                        "Passwort erfolgreich geändert",
                        it.message?.text_de,
                        "Zum Login"
                    ) {
                        goToActivity(SinginActivity::class.java, true)
                    }
                } else {
                    showBottomSheet(
                        "Zurücksetzen fehlgeschlagen",
                        it.message?.text_de,
                        "Verstanden"
                    )
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })
    }

    private fun showBottomSheet(title: String, desc: String? = errorString, titleButton: String, callback: (() -> Unit)? = null) {
        val bottomSheet = CustomBottomSheet()
        bottomSheet.title = title
        bottomSheet.desc = desc.toString()
        bottomSheet.titleButton = titleButton
        bottomSheet.didTouchButtonAction = {
            callback?.invoke()
            bottomSheet.dismiss()
        }
        bottomSheet.isCancelable = false
        bottomSheet.show(supportFragmentManager, "bottomSheet")
    }

    override fun onBackPressed() {
        finish()
    }
}