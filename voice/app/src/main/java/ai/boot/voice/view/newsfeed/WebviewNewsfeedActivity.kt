package ai.boot.voice.view.newsfeed

import ai.boot.voice.R
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.common.Const
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.activity_webview_newsfeed.*

class WebviewNewsfeedActivity : BaseActivity() {

    private lateinit var myWebView: WebView
    private lateinit var btnClose: Button
    private var titleWebview: String = ""
    private lateinit var clipBoard: ClipboardManager
    private var urlWebView: String  = ""

    override fun getRootLayoutId(): Int {
        return R.layout.activity_webview_newsfeed
    }

    override fun setupView(savedInstanceState: Bundle?) {

        extraData {
            titleWebview = it.getString(Const.TITLE_WEBVIEW).toString()
            urlWebView = it.getString(Const.LINK_WEBVIEW).toString()
        }

        webview_newsfeed.loadUrl(urlWebView)
        tv_title_webview.text = titleWebview
        tv_link_webview.text = urlWebView
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        btn_close_webview_newsfeed.setOnClickListener {
            onBackPressed()
        }

        btn_copy_link.setOnClickListener {
            val clip = ClipData.newPlainText("RANDOM UUID",urlWebView)
            clipboard.setPrimaryClip(clip)
            makeToastWithMessage("Copied")
        }

//        clipboard.prima

//        webview_newsfeed.onPa
    }
}