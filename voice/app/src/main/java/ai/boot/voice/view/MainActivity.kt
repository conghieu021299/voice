package ai.boot.voice.view

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import ai.boot.voice.R
import ai.boot.voice.adapter.ViewPagerAdapter
import ai.boot.voice.base.BaseActivity
import ai.boot.voice.view.newsfeed.MainNewsfeedActivity
import ai.boot.voice.view.newsfeed.MainNewsfeedFavouriteActivity
import ai.boot.voice.view.profile.main.ProfileMainFragment
import ai.boot.voice.view.tabs.Home3Fragment
import kotlinx.android.synthetic.main.main_activity.*


class MainActivity : BaseActivity() {
    override fun setupView(savedInstanceState: Bundle?) {
        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.tab_newfeed -> vpContainer.currentItem = 0
                R.id.tab_favorite -> vpContainer.currentItem = 1
                R.id.tab_notification -> vpContainer.currentItem = 2
                R.id.tab_profile -> vpContainer.currentItem = 3
            }
            return@setOnNavigationItemSelectedListener true
        }

        vpContainer.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                bottom_navigation.menu.getItem(position).isChecked = true
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
        setupViewPager(vpContainer)
    }

    override fun getRootLayoutId(): Int {
        return R.layout.main_activity
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(MainNewsfeedActivity.newInstance())
        adapter.addFragment(MainNewsfeedFavouriteActivity.newInstance())
        adapter.addFragment(Home3Fragment.newInstance())
        adapter.addFragment(ProfileMainFragment.newInstance())
        viewPager.adapter = adapter;
        viewPager.offscreenPageLimit = 4
    }

}
