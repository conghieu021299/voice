package ai.boot.voice.view.profile.main

import ai.boot.voice.api.ApiBuilder
import ai.boot.voice.api.MessageModel
import ai.boot.voice.api.ResponseModel
import ai.boot.voice.base.BaseViewModel
import ai.boot.voice.model.InfomationProfileResponse
import ai.boot.voice.model.UploadAvatarResponse
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ProfileMainFragmentViewModel: BaseViewModel() {
    private val userResponse = MutableLiveData<ResponseModel<InfomationProfileResponse>>()

    fun showUserInfo(): MutableLiveData<ResponseModel<InfomationProfileResponse>> {
        return userResponse
    }

    fun calApiGetInfomationProfile() {
        disposables.add(
            ApiBuilder.getWebService().getInfomationProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe { showLoading(true) }
//                .doFinally { showLoading(false) }
                .subscribe({
                    userResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<InfomationProfileResponse>(message = message)
                    userResponse.value = response
                })
        )
    }

    fun calApiGetInfomationProfileLoading() {
        disposables.add(
            ApiBuilder.getWebService().getInfomationProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    userResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = ResponseModel<InfomationProfileResponse>(message = message)
                    userResponse.value = response
                })
        )
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}