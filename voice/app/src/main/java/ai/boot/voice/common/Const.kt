package ai.boot.voice.common

import ai.boot.voice.BuildConfig

class Const {
    companion object {
        val KEY_EXTRA_DATA: String = "data"
        val KEY_USER: String = "KEY_USER"
        val KEY_EMAIL_LIST: String = "KEY_EMAIL_LIST"
        var SERVER_ADDRESS = BuildConfig.API_URL
        val KEY_TOKEN: String = "KEY_TOKEN"
        val SUCCESS: String = "success"
        val ID_ERROR: String = "-1"
        val NEW_PASSWORD_REQUIRED: String = "NEW_PASSWORD_REQUIRED"
        val FORGOT_PASSWORD: String = "FORGOT_PASSWORD"
        val CODE_CALLBACK_SEARCHCATEGORY: Int = 111
        val CODE_CALLBACK_CHANGEDINFO: Int = 1111
        val CODE_CALLBACK_ONTOPIC_NEWSFEED: Int = 222
        val KEY_PROFILE_MODEL: String = "KEY_PROFILE_MODEL"
        val YYYYMMDD: String = "yyyy-MM-dd"
        val DDMMYYYY: String = "dd.MM.yyyy"

        val TITLE_WEBVIEW: String = "TITLE_WEBVIEW"
        val LINK_WEBVIEW: String = "LINK_WEBVIEW"
        val SAVED_ARTICLE_FROM_MAIN_NEWSFEED: String = "SAVED_ARTICLE_FROM_MAIN_NEWSFEED"
        val SAVED_ARTICLE_FROM_SAVED_NEWSFEED: String = "SAVED_ARTICLE_FROM_SAVED_NEWSFEED"

        // regex
        val emailRegex: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$"
        val passwordRegex: String = "^(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9!@#$%^&*()\\-_=+{}|?>.<,:;]{8,16}$"

    }
}