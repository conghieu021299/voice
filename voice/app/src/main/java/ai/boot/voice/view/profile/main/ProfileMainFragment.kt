package ai.boot.voice.view.profile.main

import ai.boot.voice.MyApplication
import ai.boot.voice.R
import ai.boot.voice.adapter.RepositorieAdapter
import ai.boot.voice.base.BaseFragment
import ai.boot.voice.common.Const
import ai.boot.voice.common.extensions.CircleTransform
import ai.boot.voice.customview.CustomBottomSheet
import ai.boot.voice.model.InfomationProfileResponse
import ai.boot.voice.model.RepositoriesEntity
import ai.boot.voice.utils.SharePreferenceUtils
import ai.boot.voice.utils.Utils
import ai.boot.voice.view.MainActivity
import ai.boot.voice.view.forgot.password.ForgotPasswordActivity
import ai.boot.voice.view.newfeed.category.CategoryNFViewModel
import ai.boot.voice.view.profile.main.infomation.PFInfomationActivity
import ai.boot.voice.view.profile.main.upload.avatar.UploadAvatarProfileActivity
import ai.boot.voice.view.reset.password.ResetPasswordActivity
import ai.boot.voice.view.signin.SinginActivity
import ai.boot.voice.view.tabs.Home1Fragment
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile_main_fragment.*
import kotlinx.android.synthetic.main.fragment_home.*

import com.squareup.picasso.Transformation


class ProfileMainFragment : BaseFragment() {
    companion object {
        fun newInstance(): ProfileMainFragment {
            return ProfileMainFragment()
        }
    }

    private lateinit var viewModel: ProfileMainFragmentViewModel
    private var profileModel: InfomationProfileResponse = InfomationProfileResponse()
    val REQUEST_CODE_CAPTURE = 200
    val REQUEST_CODE_PICK = 201
    var isFirstTimeGoTo: Boolean = false

    override fun getRootLayoutId(): Int {
        return R.layout.activity_profile_main_fragment
    }

    override fun setupViewModel() {

    }

    override fun setupUI(view: View) {
        viewModel = ViewModelProviders.of(this).get(ProfileMainFragmentViewModel::class.java)
        setObserveLive(viewModel)

        viewModel.showUserInfo().observe(this, Observer {
            if (it.message?.id != Const.ID_ERROR) {
                if (it.message?.status == Const.SUCCESS) {
                    if (it.data != null) {
                        setData(it.data)
                    }
                }
            } else {
                makeToastSomethingWentWrong()
            }
        })

        viewModel.calApiGetInfomationProfile()

        iv_change_avatar.setOnClickListener {
            val bottomSheet = CustomBottomSheet()
            bottomSheet.title = "Foto hochladen"
            bottomSheet.desc = ""
            bottomSheet.titleButton = "Vom Fotos auswählen"
            bottomSheet.titleAcceptButton2 = "Ein Foto machen"
            bottomSheet.isUsingTwoAcceptButton = true
            bottomSheet.didTouchButtonAction = {
                if (askForPermissionsCamera()) {
                    val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(cameraIntent, REQUEST_CODE_CAPTURE)
                    bottomSheet.dismiss()
                }
            }
            bottomSheet.didTouchButtonAccept2Action = {
                if (askForPermissionsPick() && askForPermissionsCamera()) {
                    val gallery =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                    startActivityForResult(gallery, REQUEST_CODE_PICK)
                    bottomSheet.dismiss()
                }
            }
            bottomSheet.show(childFragmentManager, "bottomSheet")
        }

        rl_logout.setOnClickListener {
            SharePreferenceUtils.getInstances().clearUserInfo()
            goToActivity(SinginActivity::class.java, true)
            requireActivity().finish()
        }

        rl_infomation.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelable(Const.KEY_PROFILE_MODEL, profileModel)
            goToActivity(PFInfomationActivity::class.java, bundle = bundle)
        }
    }

    private fun setData(model: InfomationProfileResponse?) {
        if (model != null) {
            profileModel = model
            val firstname = model.first_name
            val lastname = model.last_name
            val image = model.avatar
            tv_fullname.setText("${firstname}\n${lastname}")
            Picasso.get().load(image)
                .resize(120, 120)
                .placeholder(R.mipmap.avatar_placeholder)
                .error(R.mipmap.avatar_placeholder)
                .transform(CircleTransform())
                .centerCrop().into(iv_avatar)
        }
    }

    fun isPermissionsAllowed(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    fun askForPermissionsCamera(): Boolean {
        if (!isPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.CAMERA
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CAMERA),
                    202
                )
            }
            return false
        }
        return true
    }

    fun askForPermissionsPick(): Boolean {
        if (!isPermissionsAllowed()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    203
                )
            }
            return false
        }
        return true
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton("App Settings",
                DialogInterface.OnClickListener { dialogInterface, i ->
                    // send to app settings if permission is denied permanently
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", requireContext().packageName, null)
                    intent.data = uri
                    startActivity(intent)
                })
            .setNegativeButton("Cancel", null)
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var image: Uri? = null
        if (resultCode == Activity.RESULT_OK && data != null) {
            if (requestCode == REQUEST_CODE_CAPTURE) {
                val dataBitmap = data.extras?.get("data") as Bitmap
                image = Utils.getImageUri(requireContext(), dataBitmap)
            }

            if (requestCode == REQUEST_CODE_PICK) {
                image = data?.data
            }

            if (image != null) {
                Picasso.get().load(image)
                    .resize(120, 120)
                    .placeholder(R.mipmap.avatar_placeholder)
                    .error(R.mipmap.avatar_placeholder)
                    .transform(CircleTransform())
                    .centerCrop().into(iv_avatar)

                val bundle = Bundle()
                bundle.putString("UriImage", image.toString())
                goToActivity(UploadAvatarProfileActivity::class.java, bundle = bundle)
            }
        }
    }

    private fun showBottomSheet(title: String, desc: String? = "", titleButton: String) {
        val bottomSheet = CustomBottomSheet()
        bottomSheet.title = title
        bottomSheet.desc = desc.toString()
        bottomSheet.titleButton = titleButton
        bottomSheet.didTouchButtonAction = {
            bottomSheet.dismiss()
        }
        bottomSheet.show(requireActivity().supportFragmentManager, "bottomSheet")
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            viewModel.calApiGetInfomationProfile()
        }
    }
}