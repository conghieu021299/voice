package ai.boot.voice.view.forgot.password

import ai.boot.voice.api.ApiBuilder
import ai.boot.voice.api.MessageModel
import ai.boot.voice.api.MessageResponse
import ai.boot.voice.base.BaseViewModel
import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ForgotPasswordViewModel: BaseViewModel() {
    private val userResponse = MutableLiveData<MessageResponse>()

    fun showUserInfo(): MutableLiveData<MessageResponse> {
        return userResponse
    }

    fun callApiForgotPassword(input: String) {
        disposables.add(
            ApiBuilder.getWebService().getForgotPassword(input)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { showLoading(true) }
                .doFinally { showLoading(false) }
                .subscribe({
                    userResponse.value = it
                }, {
                    val message = MessageModel(id = "-1")
                    val response = MessageResponse(message = message)
                    userResponse.value = response
                })
        )
    }

    override fun onCleared() {
        Log.d("UserViewModel", "onCleared")
        disposables.clear()
    }
}