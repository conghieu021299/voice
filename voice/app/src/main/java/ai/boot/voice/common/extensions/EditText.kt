package ai.boot.voice.common.extensions

import android.text.InputFilter
import android.util.Log
import android.widget.EditText


fun EditText.removeSpaceChar(maxLength: Int) {
    val filter = InputFilter { source, start, end, dest, dstart, dend ->
        for (i in start until end) {
            if (Character.isWhitespace(source[i])) {
                return@InputFilter ""
            }
        }
        null
    }



    filters = arrayOf(filter, InputFilter.LengthFilter(maxLength))
}