package ai.boot.voice

import ai.boot.voice.common.extensions.isEmailValid
import ai.boot.voice.common.extensions.isNameValid
import ai.boot.voice.common.extensions.isValidPassword
import ai.boot.voice.view.request.account.RequestAccountActivity
import ai.boot.voice.view.reset.password.ResetPasswordActivity
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class RequestAccountTest {
    private var requestAccountActivity: RequestAccountActivity? = null

    @Before
    @Throws(Exception::class)
    open fun setUp() {
        requestAccountActivity = Robolectric.buildActivity(RequestAccountActivity::class.java).create().get()
    }

    @Test
    @Throws(Exception::class)
    open fun checkActivityNotNull() {
        Assert.assertNotNull(requestAccountActivity)
    }

    @Test
    @Throws(Exception::class)
    open fun checkValidateNameField() {
        val input: String = "Test Test"
        Assert.assertTrue(input.isNameValid())
    }

    @Test
    @Throws(Exception::class)
    open fun checkValidateEmailField() {
        val input: String = "Test@Test.com"
        Assert.assertTrue(input.isEmailValid())
    }
}