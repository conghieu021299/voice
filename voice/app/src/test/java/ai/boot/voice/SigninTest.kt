package ai.boot.voice

import ai.boot.voice.common.extensions.isEmailValid
import ai.boot.voice.common.extensions.isValidPassword
import ai.boot.voice.view.signin.SinginActivity
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import java.util.regex.Pattern


@RunWith(RobolectricTestRunner::class)
open class SigninTest {
    private var signinActivity: SinginActivity? = null

    @Before
    @Throws(Exception::class)
    open fun setUp() {
        signinActivity = Robolectric.buildActivity(SinginActivity::class.java).create().get()
    }

    @Test
    @Throws(Exception::class)
    open fun checkActivityNotNull() {
        Assert.assertNotNull(signinActivity)
    }

    @Test
    @Throws(Exception::class)
    open fun checkValidateEmailField() {
        val input: String = "Test@Test.com"
        Assert.assertTrue(input.isEmailValid())
    }

    @Test
    @Throws(Exception::class)
    open fun checkValidatePasswordField() {
        val input: String = "Admin@1234"
        Assert.assertTrue(input.isValidPassword())
    }
}