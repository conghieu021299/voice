package ai.boot.voice

import ai.boot.voice.common.extensions.isEmailValid
import ai.boot.voice.common.extensions.isValidPassword
import ai.boot.voice.view.newfeed.category.CategoryNFActivity
import ai.boot.voice.view.signin.SinginActivity
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
open class NFCategoryTest {
    private var signinActivity: CategoryNFActivity? = null

    @Before
    @Throws(Exception::class)
    open fun setUp() {
        signinActivity = Robolectric.buildActivity(CategoryNFActivity::class.java).create().get()
    }

    @Test
    @Throws(Exception::class)
    open fun checkActivityNotNull() {
        Assert.assertNotNull(signinActivity)
    }

    @Test
    @Throws(Exception::class)
    open fun checkCallApiGetCategory() {
        val input: String = "Test@Test.com"
        Assert.assertTrue(input.isEmailValid())
    }
}