package ai.boot.voice

import ai.boot.voice.common.extensions.isEmailValid
import ai.boot.voice.common.extensions.isValidPassword
import ai.boot.voice.view.reset.password.ResetPasswordActivity
import ai.boot.voice.view.signin.SinginActivity
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
open class RecoveryPasswordTest {
    private var signinActivity: ResetPasswordActivity? = null

    @Before
    @Throws(Exception::class)
    open fun setUp() {
        signinActivity = Robolectric.buildActivity(ResetPasswordActivity::class.java).create().get()
    }

    @Test
    @Throws(Exception::class)
    open fun checkActivityNotNull() {
        Assert.assertNotNull(signinActivity)
    }

    @Test
    @Throws(Exception::class)
    open fun checkValidateVerificationField() {
        val input: String = "234534"
        Assert.assertTrue(input.count() >1 && input.count() < 20)
    }

    @Test
    @Throws(Exception::class)
    open fun checkValidatePasswordField() {
        val input: String = "Admin@1234"
        Assert.assertTrue(input.isValidPassword())
    }
}